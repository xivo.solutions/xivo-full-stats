DROP TABLE IF EXISTS "cel" CASCADE;
CREATE TABLE "cel" (
 "id" serial ,
 "eventtype" VARCHAR (30),
 "eventtime" timestamp,
 "userdeftype" VARCHAR(255),
 "cid_name" VARCHAR (80),
 "cid_num" VARCHAR (80),
 "cid_ani" VARCHAR (80),
 "cid_rdnis" VARCHAR (80),
 "cid_dnid" VARCHAR (80),
 "exten" VARCHAR (80),
 "context" VARCHAR (80),
 "channame" VARCHAR (80),
 "appname" VARCHAR (80),
 "appdata" VARCHAR (512),
 "amaflags" int,
 "accountcode" VARCHAR (20),
 "peeraccount" VARCHAR (20),
 "uniqueid" VARCHAR (150),
 "linkedid" VARCHAR (150),
 "userfield" VARCHAR (255),
 "peer" VARCHAR (80),
 "extra" TEXT,
 "call_log_id" INTEGER DEFAULT NULL,
 PRIMARY KEY("id")
);

CREATE INDEX "cel__idx__uniqueid" ON "cel"("uniqueid");
CREATE INDEX "cel__idx__eventtime" ON "cel"("eventtime");
CREATE INDEX "cel__idx__call_log_id" ON "cel" ("call_log_id");