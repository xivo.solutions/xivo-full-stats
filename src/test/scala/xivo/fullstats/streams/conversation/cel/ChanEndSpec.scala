package xivo.fullstats.streams.conversation.cel

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import akka.stream.testkit.scaladsl.TestSink
import org.joda.time.DateTime
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model._
import xivo.fullstats.streams.conversation.ConversationRepository
import xivo.fullstats.testutils.{CallTestUtils, DBTest}

class ChanEndSpec extends DBTest(List("xc_call_channel")) {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    implicit val conversationRepository = new ConversationRepository

    val startTime = format.parseDateTime("2019-01-01 12:00:00")
    val endTimeCaller = format.parseDateTime("2019-01-01 12:05:00")
    val endTimeCallee = format.parseDateTime("2019-01-01 12:05:10")
    val uniqueId = "123456.789"
    val linkedId = "987654.321"
    val channelName = "SIP/larerx9a-00000008"
    val callChannel = CallTestUtils.createCallChannel(id = None, startTime = startTime, endTime = Some(endTimeCaller),
      uniqueId = uniqueId, termination = None)

    val chanEnd = new ChanEnd
    val flowUnderTest = chanEnd.flow

    val testSink = TestSink.probe[Conversation]
    def streamUnderTest(cel: ChanEndCel) = Source(List(cel)).via(flowUnderTest)

    val cel = EmptyObjectProvider.emptyCel().copy(id = 1, eventTime = endTimeCaller, linkedId = linkedId, chanName = channelName,
      uniqueId = uniqueId, eventType = "CHAN_END", context = "default", appName = "AppDial")
  }

  "ChanEnd Flow" should "create one conversation" in new Helper {
    val chanEndCel = cel.copy(linkedId = "123456.789")

    val caller = callChannel.copy(uniqueId = "123456.789", answerTime = Some(new DateTime()))
    val callee = callChannel.copy(uniqueId = "987654.321", answerTime = Some(new DateTime()))

    val insertedCaller = CallChannel.insert(caller)
    val insertedCallee = CallChannel.insert(callee)

    val conversationLinks = ConversationLinks("123456.789", List("987654.321"))

    conversationRepository.addToMap("123456.789", conversationLinks)

    val expected = Conversation(None, insertedCaller.id.get, insertedCallee.id.get)

    streamUnderTest(ChanEndCel(chanEndCel)).runWith(testSink).request(2).expectNext(expected).expectComplete()
    conversationRepository.repository.isEmpty shouldBe true
  }

  it should "create two conversations" in new Helper {
    val chanEndCel = cel.copy(linkedId = "123456.789")

    val caller = callChannel.copy(uniqueId = "123456.789", answerTime = Some(new DateTime()))
    val callee = callChannel.copy(uniqueId = "987654.321", answerTime = Some(new DateTime()))
    val transferCallee = callChannel.copy(uniqueId = "111111.222", answerTime = Some(new DateTime()))

    val insertedCaller = CallChannel.insert(caller)
    val insertedCallee = CallChannel.insert(callee)
    val insertedTransferCallee = CallChannel.insert(transferCallee)

    val conversationLinks = ConversationLinks("123456.789", List("987654.321", "111111.222"))

    conversationRepository.addToMap("123456.789", conversationLinks)

    val expected1 = Conversation(None, insertedCaller.id.get, insertedCallee.id.get)
    val expected2 = Conversation(None, insertedCaller.id.get, insertedTransferCallee.id.get)

    streamUnderTest(ChanEndCel(chanEndCel)).runWith(testSink).request(3).expectNext(expected1, expected2).expectComplete()
    conversationRepository.repository.isEmpty shouldBe true
  }

  it should "not fail on empty list of links" in new Helper {
    val chanEndCel = cel.copy(linkedId = "123456.789")

    val conversationLinks = ConversationLinks("123456.789", List())

    conversationRepository.addToMap("123456.789", conversationLinks)

    streamUnderTest(ChanEndCel(chanEndCel)).runWith(testSink).request(1).expectComplete()
    conversationRepository.repository.isEmpty shouldBe true
  }

  it should "ignore agentcallback context" in new Helper {
    val chanEndCel = cel.copy(linkedId = linkedId, uniqueId = uniqueId, context = "agentcallback")

    streamUnderTest(ChanEndCel(chanEndCel)).runWith(testSink).request(1).expectComplete()
  }

  it should "ignore xuc_attended_xfer_wait context" in new Helper {
    val chanEndCel = cel.copy(linkedId = linkedId, uniqueId = uniqueId, context = "xuc_attended_xfer_wait")

    streamUnderTest(ChanEndCel(chanEndCel)).runWith(testSink).request(1).expectComplete()
  }

  it should "ignore xuc_attended_xfer_end context" in new Helper {
    val chanEndCel = cel.copy(linkedId = linkedId, uniqueId = uniqueId)

    streamUnderTest(ChanEndCel(chanEndCel)).runWith(testSink).request(1).expectComplete()
  }
}