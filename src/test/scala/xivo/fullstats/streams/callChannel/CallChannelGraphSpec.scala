package xivo.fullstats.streams.callChannel

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Keep, Sink, Source}
import akka.stream.{ActorMaterializer, FlowShape, Graph}
import akka.testkit.TestProbe
import org.scalatest.concurrent.Eventually
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model.{CallChannel, CallEvent}
import xivo.fullstats.testutils.{CallTestUtils, DBTest}

import scala.concurrent.duration._

class CallChannelGraphSpec extends DBTest(List("cel", "xc_call_channel")) with Eventually {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()
    implicit val repository: CallChannelRepository = new CallChannelRepository

    val startTime = format.parseDateTime("2019-01-01 12:00:00")
    val endTime = format.parseDateTime("2019-01-01 12:05:00")
    val uniqueId = "123456.789"
    val channelName = "SIP/larerx9a-00000008"

    val callChannel = CallTestUtils.createCallChannel(id = None, startTime = startTime, endTime = None,
      uniqueId = uniqueId, termination = None)

    val probe = TestProbe()

    val cel = EmptyObjectProvider.emptyCel().copy(id = 1, eventTime = startTime, linkedId = uniqueId, chanName = channelName,
      uniqueId = uniqueId, eventType = "CHAN_START")
  }

  "CallChannel Graph" should "insert call channel" in new Helper {
    val celGraph = new CallChannelGraph
    val stream: Graph[FlowShape[CallEvent, CallChannel], NotUsed] = celGraph.graph

    val graphUnderTest = Source(List(cel)).viaMat(stream)(Keep.both).runWith(Sink.ignore)

    eventually(timeout(scaled(2.seconds))) {
      val res: CallChannel = repository.repository(uniqueId)

      repository.repository.isEmpty shouldBe false
      res.channelInterface shouldEqual Some("SIP/larerx9a")
    }
  }
}
