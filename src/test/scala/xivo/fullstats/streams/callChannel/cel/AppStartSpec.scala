package xivo.fullstats.streams.callChannel.cel

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import akka.stream.testkit.scaladsl.TestSink
import org.joda.time.DateTime
import org.scalatest.concurrent.Eventually
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model._
import xivo.fullstats.streams.callChannel.CallChannelRepository
import xivo.fullstats.testutils.{CallTestUtils, DBTest}

import scala.collection.mutable

class AppStartSpec extends DBTest(List("cel")) with Eventually {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    implicit val celRepository = new CallChannelRepository

    val startTime = format.parseDateTime("2019-01-01 12:00:00")
    val endTime = format.parseDateTime("2019-01-01 12:05:00")
    val uniqueId = "123456.789"
    val channelName = "SIP/larerx9a-00000008"
    val callChannel = CallTestUtils.createCallChannel(id = None, startTime = startTime, endTime = None,
      emitted = false, uniqueId = uniqueId, termination = None)

    val appStart = new AppStart
    val flowUnderTest = appStart.flow

    val testSink = TestSink.probe[CallChannel]
    def streamUnderTest(cel: AppStartCel) = Source(List(cel)).via(flowUnderTest)

    val cel = EmptyObjectProvider.emptyCel().copy(id = 1, eventTime = endTime, linkedId = uniqueId, chanName = channelName,
      uniqueId = uniqueId, eventType = "APP_START", context = "default", appName = "Dial")
  }

  "AppStart Flow" should "set call type Administrative in call repository" in new Helper {
    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(callType = CallType.Administrative, emitted = true, appName = Some(CelAppName.DialApp))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(AppStartCel(cel)).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldBe expectedMap
  }

  it should "set call type Queued in call repository" in new Helper {
    val queueCel = cel.copy(appName = "Queue")

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(callType = CallType.Queued, emitted = true, appName = Some(CelAppName.QueueApp))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(AppStartCel(queueCel)).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldBe expectedMap
  }

  it should "set call type Queued by linkedid in call repository" in new Helper {
    val queueCel = cel.copy(appName = "Queue", uniqueId = "9876564.321", linkedId = uniqueId)

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(callType = CallType.Queued, emitted = true, appName = Some(CelAppName.QueueApp))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(AppStartCel(queueCel)).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldBe expectedMap
  }

  it should "keep call type Queued in call repository" in new Helper {
    celRepository.addCallToMap(uniqueId, callChannel.copy(callType = CallType.Queued))

    val expectedCallChannel = callChannel.copy(callType = CallType.Queued, emitted = true, appName = Some(CelAppName.DialApp))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(AppStartCel(cel)).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldBe expectedMap
  }

  it should "keep call type Consultation in call repository" in new Helper {
    val queueCel = cel.copy(appName = "Queue")
    val ongoingAcdCall = callChannel.copy(callType = CallType.Consultation)

    celRepository.addCallToMap(uniqueId, ongoingAcdCall)

    val expectedCallChannel = callChannel.copy(callType = CallType.Consultation, emitted = true, appName = Some(CelAppName.QueueApp))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(AppStartCel(queueCel)).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldBe expectedMap
  }

  it should "set call direction outgoing for Dial app" in new Helper {
    val dialCel = cel.copy(uniqueId = "9876564.321")

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(callType = CallType.Administrative, emitted = true, appName = Some(CelAppName.DialApp))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(AppStartCel(dialCel)).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldBe expectedMap
  }

  it should "set call direction outgoing for Queue app" in new Helper {
    val queueCel = cel.copy(appName = "Queue", uniqueId = "9876564.321")

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(callType = CallType.Queued, emitted = true, appName = Some(CelAppName.QueueApp))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(AppStartCel(queueCel)).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldBe expectedMap
  }

  it should "ignore agentcallback context" in new Helper {
    val queueCel = cel.copy(context = "agentcallback")

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(AppStartCel(queueCel)).runWith(testSink).request(1).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "set callee_num if not defined previously when Dial event" in new Helper {
    val dialCel = cel.copy(uniqueId = "9876564.321", cidNum = "1234")
    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(calleeNum = Some("1234"), emitted = true, appName = Some(CelAppName.DialApp))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(AppStartCel(dialCel)).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldBe expectedMap
  }

  it should "not set callee_num if defined previously when Dial event" in new Helper {
    val dialCel = cel.copy(uniqueId = "9876564.321", cidNum = "4567")
    celRepository.addCallToMap(uniqueId, callChannel.copy(calleeNum = Some("1234")))

    val expectedCallChannel = callChannel.copy(calleeNum = Some("1234"), emitted = true, appName = Some(CelAppName.DialApp))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(AppStartCel(dialCel)).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldBe expectedMap
  }

  it should "set consultation call while dialing a Queue when already in call (acd)" in new Helper {
    val queueCel = cel.copy(appName = "Queue")

    val queuedCallChannel = callChannel.copy(uniqueId = "121212.222", endTime = None, userId = Some(1), callType = CallType.Queued, answerTime = Some(new DateTime()))
    val insertedQueuedCallChannel = CallChannel.insert(queuedCallChannel)

    val userCallChannel = callChannel.copy(uniqueId = "232323.333", endTime = None, userId = Some(1), callType = CallType.Acd, answerTime = Some(new DateTime()))
    val insertedUserCallChannel = CallChannel.insert(userCallChannel)

    celRepository.addCallToMap("121212.222", insertedQueuedCallChannel)
    celRepository.addCallToMap("232323.333", insertedUserCallChannel)
    celRepository.addCallToMap(uniqueId, callChannel.copy(userId = Some(1)))

    val expectedCallChannel = callChannel.copy(userId = Some(1), callType = CallType.Consultation, emitted = true, appName = Some(CelAppName.QueueApp))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel, "121212.222"-> insertedQueuedCallChannel, "232323.333" -> insertedUserCallChannel)

    streamUnderTest(AppStartCel(queueCel)).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "set consultation call while dialing when already in call (non-acd)" in new Helper {

    val firstCallChannel = CallChannel.insert(
      callChannel.copy(
        uniqueId = "121212.222",
        endTime = None,
        userId = Some(1),
        callType = CallType.Administrative,
        answerTime = Some(new DateTime()))
    )
 
    val secondCallChannel = CallChannel.insert(
      callChannel.copy(
        uniqueId = "232323.333",
        endTime = None,
        userId = Some(1),
        callType = CallType.Administrative,
        answerTime = Some(new DateTime()))
    )

    celRepository.addCallToMap(firstCallChannel.uniqueId, firstCallChannel)
    celRepository.addCallToMap(secondCallChannel.uniqueId, secondCallChannel)

    val dialCel = cel.copy(
      uniqueId = secondCallChannel.uniqueId,
      linkedId = secondCallChannel.uniqueId,
      appName = "Dial")


    val secondCallAsConsultCall = secondCallChannel.copy(
      callType = CallType.Consultation,
      emitted = true,
      appName = Some(CelAppName.DialApp)
    )
    val expectedMap = mutable.HashMap(
      firstCallChannel.uniqueId -> firstCallChannel,
      secondCallAsConsultCall.uniqueId -> secondCallAsConsultCall
    )

    streamUnderTest(AppStartCel(dialCel))
      .runWith(testSink)
      .request(1)
      .expectNext(secondCallAsConsultCall)
      .expectComplete()

    celRepository.repository shouldEqual expectedMap
  }

  it should "do not set consultation call if user_id is not set" in new Helper {

    val firstCallChannel = CallChannel.insert(
      callChannel.copy(
        uniqueId = "121212.222",
        endTime = None,
        userId = None,
        callType = CallType.Administrative,
        answerTime = Some(new DateTime()))
    )

    val secondCallChannel = CallChannel.insert(
      callChannel.copy(
        uniqueId = "232323.333",
        endTime = None,
        userId = None,
        callType = CallType.Administrative,
        answerTime = Some(new DateTime()))
    )

    celRepository.addCallToMap(firstCallChannel.uniqueId, firstCallChannel)
    celRepository.addCallToMap(secondCallChannel.uniqueId, secondCallChannel)

    val dialCel = cel.copy(
      uniqueId = secondCallChannel.uniqueId,
      linkedId = secondCallChannel.uniqueId,
      appName = "Dial")


    val secondCallAsEmitted = secondCallChannel.copy(
      emitted = true,
      appName = Some(CelAppName.DialApp)
    )
    val expectedMap = mutable.HashMap(
      firstCallChannel.uniqueId -> firstCallChannel,
      secondCallAsEmitted.uniqueId -> secondCallAsEmitted
    )

    streamUnderTest(AppStartCel(dialCel))
      .runWith(testSink)
      .request(1)
      .expectNext(secondCallAsEmitted)
      .expectComplete()

    celRepository.repository shouldEqual expectedMap
  }

  it should "set app name to Dial" in new Helper {
    celRepository.addCallToMap(uniqueId, callChannel)
    val celDial = cel.copy(appName = "Dial")

    val expectedCallChannel = callChannel.copy(callType = CallType.Administrative, emitted = true, appName = Some(CelAppName.DialApp))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(AppStartCel(celDial)).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldBe expectedMap
  }

  it should "set app name to Queue" in new Helper {
    celRepository.addCallToMap(uniqueId, callChannel)
    val celQueue = cel.copy(appName = "Queue")

    val expectedCallChannel = callChannel.copy(callType = CallType.Queued, emitted = true, appName = Some(CelAppName.QueueApp))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(AppStartCel(celQueue)).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldBe expectedMap
  }
}
