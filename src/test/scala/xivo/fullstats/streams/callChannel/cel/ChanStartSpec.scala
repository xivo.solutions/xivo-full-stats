package xivo.fullstats.streams.callChannel.cel

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import akka.stream.testkit.scaladsl.TestSink
import anorm.SQL
import org.joda.time.DateTime
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model._
import xivo.fullstats.streams.callChannel.CallChannelRepository
import xivo.fullstats.testutils.{CallTestUtils, CelTestUtils, DBTest}

import scala.collection.mutable

class ChanStartSpec extends DBTest(List("cel", "extensions", "xc_queue_call", "xc_call_channel")) {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    implicit val celRepository = new CallChannelRepository

    val startTime = format.parseDateTime("2019-01-01 12:00:00")
    val uniqueId = "123456.789"
    val channelName = "SIP/larerx9a-00000008"

    val callChannel = CallTestUtils.createCallChannel(id = None, startTime = startTime, endTime = None,
      uniqueId = uniqueId, termination = None)

    val queueCall = CallTestUtils.createQueueCall(uniqueId = "987654.321")

    val testSink = TestSink.probe[CallChannel]

    val chanStart = new ChanStart
    val flowUnderTest = chanStart.flow

    def streamUnderTest(cel: ChanStartCel) = Source(List(cel)).via(flowUnderTest)

    val cel = EmptyObjectProvider.emptyCel().copy(id = 1, eventTime = startTime,
      linkedId = uniqueId, chanName = channelName, uniqueId = uniqueId, eventType = "CHAN_START")

    def insertExtension(e: Extension): Option[Long] = SQL("""INSERT INTO extensions(commented, context, exten, type, typeval)
      VALUES ({commented}, {context}, {exten}, {type}, {typeval})""")
      .on('commented -> e.commented, 'context -> e.context, 'exten -> e.exten, 'type -> e.extType, 'typeval -> e.typeval)
      .executeInsert()
  }

  "ChanStart Flow" should "set asterisk id (uniqueid) in call channel" in new Helper {
    val expectedCallChannel = callChannel.copy(channelInterface = Some("SIP/larerx9a"), scope = CallScope.External)
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(ChanStartCel(cel)).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "set acd call type (queued) in call channel" in new Helper {
    val extension = Extension(None, 0, "default", "2001", "user", "1")
    insertExtension(extension)
    val originatorUniqueId = uniqueId
    val originatorCallChannel = callChannel.copy(callType = CallType.Queued, appName = Some(CelAppName.QueueApp))
    val notYetAcdCallChannel = callChannel.copy(uniqueId = "987654.321")

    celRepository.addCallToMap(originatorUniqueId, originatorCallChannel)
    celRepository.addCallToMap("987654.321", notYetAcdCallChannel)

    val expectedCallChannel = notYetAcdCallChannel.copy(callType = CallType.Acd, channelInterface = Some("SIP/larerx9a"), userId = Some(1), callerNum = Some("2001"))
    val expectedMap = mutable.HashMap(originatorUniqueId -> originatorCallChannel, "987654.321" -> expectedCallChannel)

    val acdCel = cel.copy(cidNum = "2001", uniqueId = "987654.321", linkedId = originatorUniqueId)

    streamUnderTest(ChanStartCel(acdCel)).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "set acd call type (consultation) in call channel" in new Helper {
    val extension = Extension(None, 0, "default", "2001", "user", "1")
    insertExtension(extension)
    val originatorUniqueId = uniqueId
    val originatorCallChannel = callChannel.copy(callType = CallType.Consultation, appName = Some(CelAppName.QueueApp))
    val notYetAcdCallChannel = callChannel.copy(uniqueId = "987654.321")

    celRepository.addCallToMap(originatorUniqueId, originatorCallChannel)
    celRepository.addCallToMap("987654.321", notYetAcdCallChannel)

    val expectedCallChannel = notYetAcdCallChannel.copy(callType = CallType.Acd, channelInterface = Some("SIP/larerx9a"), userId = Some(1), callerNum = Some("2001"))
    val expectedMap = mutable.HashMap(originatorUniqueId -> originatorCallChannel, "987654.321" -> expectedCallChannel)

    val acdCel = cel.copy(cidNum = "2001", uniqueId = "987654.321", linkedId = originatorUniqueId)

    streamUnderTest(ChanStartCel(acdCel)).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "not set acd call type without user id" in new Helper {
    val originatorUniqueId = uniqueId
    val originatorCallChannel = callChannel.copy(callType = CallType.Queued)
    val notYetAcdCallChannel = callChannel.copy(uniqueId = "987654.321")

    celRepository.addCallToMap(originatorUniqueId, originatorCallChannel)
    celRepository.addCallToMap("987654.321", notYetAcdCallChannel)

    val expectedCallChannel = notYetAcdCallChannel.copy(callType = CallType.Administrative, channelInterface = Some("SIP/larerx9a"), scope = CallScope.External)
    val expectedMap = mutable.HashMap(originatorUniqueId -> originatorCallChannel, "987654.321" -> expectedCallChannel)

    val acdCel = cel.copy(uniqueId = "987654.321", linkedId = originatorUniqueId)

    streamUnderTest(ChanStartCel(acdCel)).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "set user id in call channel" in new Helper {
    val extension = Extension(None, 0, "default", "2001", "user", "1")
    insertExtension(extension)

    val celWithCidNum = cel.copy(cidNum = "2001")

    val expectedCallChannel = callChannel.copy(callerNum = Some("2001"), channelInterface = Some("SIP/larerx9a"), userId = Some(1))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(ChanStartCel(celWithCidNum)).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "parse the sip channel to peer name" in new Helper {
    val celWithChanName = cel.copy(chanName = "SIP/larerx9a-0123456d")

    val expectedCallChannel = callChannel.copy(channelInterface = Some("SIP/larerx9a"), scope = CallScope.External)
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(ChanStartCel(celWithChanName)).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "parse the trunk channel to peer name" in new Helper {
    val celWithChanName = cel.copy(chanName = "SIP/trunk-maq-xivo-0000004e")

    val expectedCallChannel = callChannel.copy(channelInterface = Some("SIP/trunk-maq-xivo"), scope = CallScope.External)
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(ChanStartCel(celWithChanName)).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "handle missing sip channel" in new Helper {
    val celWithNoChanName = cel.copy(chanName = "")

    val expectedCallChannel = callChannel.copy(channelInterface = None, scope = CallScope.External)
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(ChanStartCel(celWithNoChanName)).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "set queue id in acd call channel" in new Helper {
    val extension = Extension(None, 0, "default", "2001", "user", "1")
    insertExtension(extension)

    val calleeCel = cel.copy(cidNum = "2001", uniqueId = uniqueId, linkedId = "987654.321")
    val appStartCel = cel.copy(id = 2, uniqueId = "121212.222", linkedId = "987654.321",
      eventType = "APP_START", appName = "Queue", context = "queue")

    val queueCallWithDifferentAsteriskId = queueCall.copy(uniqueId = "121212.222")
    val ongoingQueuedCallChannel = callChannel.copy(callType = CallType.Queued, appName = Some(CelAppName.QueueApp))

    val insertedQueueCall1 = QueueCall.insert(queueCallWithDifferentAsteriskId.copy(startTime = startTime.minusSeconds(10)))
    val insertedQueueCall2 = QueueCall.insert(queueCallWithDifferentAsteriskId.copy(startTime = startTime.minusSeconds(5)))
    val insertedCel = CelTestUtils.insertCel(appStartCel)

    celRepository.addCallToMap("987654.321", ongoingQueuedCallChannel)

    val expectedCallChannel = callChannel.copy(channelInterface = Some("SIP/larerx9a"), queueCallId = insertedQueueCall2.id, callType = CallType.Acd, userId = Some(1), callerNum = Some("2001"))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel, "987654.321" -> ongoingQueuedCallChannel)

    streamUnderTest(ChanStartCel(calleeCel)).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "not set queue id in non-acd call channel" in new Helper {
    val calleeCel = cel.copy(uniqueId = uniqueId, linkedId = "987654.321")
    val appStartCel = cel.copy(id = 2, uniqueId = "121212.222", linkedId = "987654.321",
      eventType = "APP_START", appName = "Queue", context = "queue")

    val queueCallWithDifferentAsteriskId = queueCall.copy(uniqueId = "121212.222")

    val expectedCallChannel = callChannel.copy(channelInterface = Some("SIP/larerx9a"), queueCallId = None, scope = CallScope.External)
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(ChanStartCel(calleeCel)).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "set original call id from ongoing acd call channel" in new Helper {
    val extension = Extension(None, 0, "default", "2001", "user", "1")
    insertExtension(extension)
    val celWithCidNum = cel.copy(cidNum = "2001")
    val ongoingCallChannel = callChannel.copy(uniqueId = "121212.222", endTime = None, userId = Some(1),
      callType = CallType.Acd, answerTime = Some(new DateTime()), appName = Some(CelAppName.QueueApp))
    val insertedCallChannel = CallChannel.insert(ongoingCallChannel)

    celRepository.addCallToMap("121212.222", insertedCallChannel)

    val expectedCallChannel = callChannel.copy(callerNum = Some("2001"), channelInterface = Some("SIP/larerx9a"), userId = Some(1), originalCallId = insertedCallChannel.id, callThreadId = insertedCallChannel.id, callThreadSeq = insertedCallChannel.callThreadSeq + 1)
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel, "121212.222"-> insertedCallChannel)

    streamUnderTest(ChanStartCel(celWithCidNum)).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "set original call id from queued call channel" in new Helper {
    val extension = Extension(None, 0, "default", "2001", "user", "1")
    insertExtension(extension)

    val celWithCidNum = cel.copy(cidNum = "2001", linkedId = "121212.222")
    val queuedCallChannel = callChannel.copy(uniqueId = "121212.222", endTime = None, userId = Some(1),
      callType = CallType.Queued, answerTime = Some(new DateTime()), appName = Some(CelAppName.QueueApp))
    val insertedCallChannel = CallChannel.insert(queuedCallChannel)

    celRepository.addCallToMap("121212.222", insertedCallChannel)

    val expectedCallChannel = callChannel.copy(callerNum = Some("2001"), channelInterface = Some("SIP/larerx9a"),
      userId = Some(1), originalCallId = insertedCallChannel.id, callType = CallType.Acd, callThreadId = insertedCallChannel.id, callThreadSeq = insertedCallChannel.callThreadSeq + 1)
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel, "121212.222"-> insertedCallChannel)

    streamUnderTest(ChanStartCel(celWithCidNum)).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "set original call id from administrative call channel" in new Helper {
    val extension = Extension(None, 0, "default", "2001", "user", "1")
    insertExtension(extension)

    val celWithCidNum = cel.copy(cidNum = "2001", linkedId = "121212.222")
    val queuedCallChannel = callChannel.copy(uniqueId = "121212.222", endTime = None, userId = Some(1),
      callType = CallType.Administrative, answerTime = Some(new DateTime()))
    val insertedCallChannel = CallChannel.insert(queuedCallChannel)

    celRepository.addCallToMap("121212.222", insertedCallChannel)

    val expectedCallChannel = callChannel.copy(callerNum = Some("2001"), channelInterface = Some("SIP/larerx9a"),
      userId = Some(1), originalCallId = insertedCallChannel.id, callType = CallType.Administrative, callThreadId = insertedCallChannel.id, callThreadSeq = insertedCallChannel.callThreadSeq + 1)
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel, "121212.222"-> insertedCallChannel)

    streamUnderTest(ChanStartCel(celWithCidNum)).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "not set original call id if userId is None" in new Helper {
    val someCallChannel = callChannel.copy(uniqueId = "121212.222", endTime = None, userId = None,
      callType = CallType.Administrative, answerTime = Some(new DateTime()))
    val insertedCallChannel = CallChannel.insert(someCallChannel)
    celRepository.addCallToMap("121212.222", insertedCallChannel)


    val celWithCidNum = cel.copy(cidNum = "0123456789", uniqueId = "121212.333", linkedId = "121212.333")


    val expectedCallChannel = callChannel.copy(callerNum = Some("0123456789"), channelInterface = Some("SIP/larerx9a"),
      userId = None,  callType = CallType.Administrative, uniqueId = "121212.333", originalCallId = None)
    val expectedMap = mutable.HashMap("121212.333" -> expectedCallChannel, "121212.222"-> insertedCallChannel)

    streamUnderTest(ChanStartCel(celWithCidNum)).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "set original call id from blind transfer (acd)" in new Helper {
    val extension = Extension(None, 0, "default", "2001", "user", "1")
    insertExtension(extension)
    CelTestUtils.insertCel(EmptyObjectProvider.emptyCel().copy(uniqueId = "232323.333", linkedId = "121212.222", eventType = "BLINDTRANSFER"))

    val celWithCidNum = cel.copy(cidNum = "2001", linkedId = "121212.222")

    val queuedCallChannel = callChannel.copy(uniqueId = "121212.222", endTime = None, userId = None, callType = CallType.Queued, answerTime = Some(new DateTime()))
    val insertedQueuedCallChannel = CallChannel.insert(queuedCallChannel)

    val acdCallChannel = callChannel.copy(uniqueId = "232323.333", endTime = None, userId = Some(1), callType = CallType.Acd, answerTime = Some(new DateTime()))
    val insertedAcdCallChannel = CallChannel.insert(acdCallChannel)

    celRepository.addCallToMap("121212.222", insertedQueuedCallChannel)

    val expectedCallChannel = callChannel.copy(callerNum = Some("2001"), channelInterface = Some("SIP/larerx9a"),
      userId = Some(1), originalCallId = insertedAcdCallChannel.id, callType = CallType.Administrative, callThreadId = insertedAcdCallChannel.id, callThreadSeq = insertedAcdCallChannel.callThreadSeq + 1)
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel, "121212.222" -> insertedQueuedCallChannel)

    streamUnderTest(ChanStartCel(celWithCidNum)).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "set original call id from blind transfer (consultation)" in new Helper {
    val extension = Extension(None, 0, "default", "2001", "user", "1")
    insertExtension(extension)
    CelTestUtils.insertCel(EmptyObjectProvider.emptyCel().copy(uniqueId = "232323.333", linkedId = "121212.222", eventType = "BLINDTRANSFER"))

    val celWithCidNum = cel.copy(cidNum = "2001", linkedId = "121212.222")

    val queuedCallChannel = callChannel.copy(uniqueId = "121212.222", endTime = None, userId = None,
      callType = CallType.Queued, answerTime = Some(new DateTime()))
    val insertedQueuedCallChannel = CallChannel.insert(queuedCallChannel)

    val consultationCallChannel = callChannel.copy(uniqueId = "232323.333", endTime = None, userId = Some(1),
      callType = CallType.Consultation, answerTime = Some(new DateTime()))
    val insertedAcdCallChannel = CallChannel.insert(consultationCallChannel)

    celRepository.addCallToMap("121212.222", insertedQueuedCallChannel)

    val expectedCallChannel = callChannel.copy(callerNum = Some("2001"), channelInterface = Some("SIP/larerx9a"),
      userId = Some(1), originalCallId = insertedAcdCallChannel.id, callType = CallType.Acd, callThreadId = insertedAcdCallChannel.id, callThreadSeq = insertedAcdCallChannel.callThreadSeq + 1)
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel, "121212.222"-> insertedQueuedCallChannel)

    streamUnderTest(ChanStartCel(celWithCidNum)).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "set call thread seq to next seq after aborted transfer" in new Helper {
    // 1. Initial Situation, User1 is on call with an Acd call and has a consultation call
    //   customerChannel("101", Queued, seq=1) ──── userChannel1("102", Acd, SIP/abcdef, seq=2) ─┬─ User 1
    //   remoteChannel2("104", Adm, seq=4)     ──── userChannel2("103", Adm, SIP/abcdef, seq=3) ─┘
    //
    // 2. User1 hangs up consultation call
    //   customerChannel("101", Queued, seq=1) ──── userChannel1("102", Acd, SIP/abcdef, seq=2) ─── User 1
    //
    // 3. User1 makes another consultation call
    //   customerChannel("101", Queued, seq=1) ──── userChannel1("102", Acd, SIP/abcdef, seq=2) ─┬─ User 1
    //                                         ──── userChannel3("105", Adm, SIP/abcdef, seq=5) ─┘
    // 4. Assert that sequence of userChannel3 must be 5

    insertExtension(Extension(None, 0, "default", "2001", "user", "1"))

    val customerChannel = CallChannel.insert(CallTestUtils.createCallChannel(
      id = None, uniqueId = "101",
      startTime = startTime, answerTime = Some(startTime), endTime = None,
      callType = CallType.Queued, termination = None))

    val userChannel1 = CallChannel.insert(CallTestUtils.createCallChannel(
      id = None, uniqueId = "102", originalCallId = customerChannel.id,
      callThreadId = customerChannel.id, callThreadSeq = 2,
      startTime = startTime, answerTime = Some(startTime), endTime = None,
      callType = CallType.Acd, termination = None,
      userId = Some(1), device = Some("SIP/abcdef")
    ))

    val userChannel2 = CallChannel.insert(CallTestUtils.createCallChannel(
      id = None, uniqueId = "103", originalCallId = userChannel1.id,
      callThreadId = customerChannel.id, callThreadSeq = 3,
      startTime = startTime.plusMinutes(1), answerTime = Some(startTime.plusMinutes(1)), endTime = Some(startTime.plusMinutes(2)),
      callType = CallType.Administrative, termination = Some(CallTermination.Hangup),
      userId = Some(1), device = Some("SIP/abcdef")
    ))

    val remoteChannel2 = CallChannel.insert(CallTestUtils.createCallChannel(
      id = None, uniqueId = "104", originalCallId = userChannel2.id,
      callThreadId = customerChannel.id, callThreadSeq = 4,
      startTime = startTime.plusMinutes(1), answerTime = Some(startTime.plusMinutes(1)), endTime = Some(startTime.plusMinutes(2)),
      callType = CallType.Administrative, termination = Some(CallTermination.RemoteHangup),
      userId = None, device = None
    ))

    celRepository.addCallToMap(customerChannel.uniqueId, customerChannel)
    celRepository.addCallToMap(userChannel1.uniqueId, userChannel1)

    val chanStartCel = EmptyObjectProvider.emptyCel().copy(id = 1, eventTime = startTime.plusMinutes(3),
      linkedId = "105", chanName = "SIP/abcdef-0003", uniqueId = "105", eventType = "CHAN_START",
      cidNum = "2001")

    val userChannel3 = streamUnderTest(ChanStartCel(chanStartCel))
      .runWith(testSink)
      .request(1)
      .expectNext()

    userChannel3.uniqueId shouldBe ("105")
    userChannel3.callThreadId shouldBe (userChannel1.callThreadId)
    userChannel3.originalCallId shouldBe (userChannel1.id)
    userChannel3.callThreadSeq shouldBe (5)

  }

  it should "not set original call id in unanswered call channel" in new Helper {
    val extension = Extension(None, 0, "default", "2001", "user", "1")
    insertExtension(extension)
    val celWithCidNum = cel.copy(cidNum = "2001")
    val ongoingCallChannel = callChannel.copy(uniqueId = "121212.222", endTime = None, userId = Some(1), answerTime = None)
    val insertedCallChannel = CallChannel.insert(ongoingCallChannel)

    celRepository.addCallToMap("121212.222", insertedCallChannel)

    val expectedCallChannel = callChannel.copy(callerNum = Some("2001"), channelInterface = Some("SIP/larerx9a"), userId = Some(1), originalCallId = None)
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel, "121212.222"-> insertedCallChannel)

    streamUnderTest(ChanStartCel(celWithCidNum)).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "not set original call id in unanswered acd call channel" in new Helper {
    val extension = Extension(None, 0, "default", "2001", "user", "1")
    insertExtension(extension)
    val celWithCidNum = cel.copy(cidNum = "2001")
    val ongoingCallChannel = callChannel.copy(uniqueId = "121212.222", endTime = None, userId = Some(1), callType = CallType.Acd, answerTime = None)
    val insertedCallChannel = CallChannel.insert(ongoingCallChannel)

    celRepository.addCallToMap("121212.222", insertedCallChannel)

    val expectedCallChannel = callChannel.copy(callerNum = Some("2001"), channelInterface = Some("SIP/larerx9a"), userId = Some(1), originalCallId = None)
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel, "121212.222"-> insertedCallChannel)

    streamUnderTest(ChanStartCel(celWithCidNum)).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "set original call id in administrative call channel" in new Helper {
    val extension = Extension(None, 0, "default", "2001", "user", "1")
    insertExtension(extension)
    val celWithCidNum = cel.copy(cidNum = "2001")
    val ongoingCallChannel = callChannel.copy(uniqueId = "121212.222", endTime = None, userId = Some(1), answerTime = Some(new DateTime()))
    val insertedCallChannel = CallChannel.insert(ongoingCallChannel)

    celRepository.addCallToMap("121212.222", insertedCallChannel)

    val expectedCallChannel = callChannel.copy(callerNum = Some("2001"), channelInterface = Some("SIP/larerx9a"), userId = Some(1), originalCallId = insertedCallChannel.id, callThreadId = insertedCallChannel.id, callThreadSeq = insertedCallChannel.callThreadSeq + 1)
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel, "121212.222"-> insertedCallChannel)

    streamUnderTest(ChanStartCel(celWithCidNum)).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "not add call channel if Local channel name" in new Helper {
    val localCel = cel.copy(chanName = "Local/1012@default-0000000c;2")
    val expectedCallChannel = callChannel.copy(channelInterface = Some("SIP/larerx9a"), userId = Some(1), originalCallId = Some(1L))

    streamUnderTest(ChanStartCel(localCel)).runWith(testSink).request(1).expectComplete()
    celRepository.repository.isEmpty shouldBe true
  }

  it should "add caller and callee numbers to the call channel repository" in new Helper {
    val celWithExten = cel.copy(cidNum = "1000", exten = "1234")

    val expectedCallChannel = callChannel.copy(callerNum = Some("1000"), calleeNum = Some("1234"), channelInterface = Some("SIP/larerx9a"))
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(ChanStartCel(celWithExten)).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "not fill caller and callee numbers if empty string or s asterisk operand" in new Helper {
    val celWithExten = cel.copy(cidNum = "    ", exten = "s")

    val expectedCallChannel = callChannel.copy(callerNum = None, calleeNum = None, channelInterface = Some("SIP/larerx9a"), scope = CallScope.External)
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(ChanStartCel(celWithExten)).runWith(testSink).request(1).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }

  it should "set external scope if no userId " in new Helper {

    val celWithoutCidNum = cel.copy(cidNum = "")

    val expectedCallChannel = callChannel.copy(
      scope = CallScope.External,
      callerNum = None,
      channelInterface = Some("SIP/larerx9a"),
      userId = None
    )
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    streamUnderTest(ChanStartCel(celWithoutCidNum)).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository shouldEqual expectedMap
  }
}
