package xivo.fullstats.streams.callChannel.cel

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import akka.stream.testkit.scaladsl.TestSink
import akka.testkit.TestProbe
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model._
import xivo.fullstats.streams.callChannel.CallChannelRepository
import xivo.fullstats.testutils.{CallTestUtils, DBTest}

class LinkedIdEndSpec extends DBTest(List("cel")) {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    implicit val celRepository = new CallChannelRepository

    val startTime = format.parseDateTime("2019-01-01 12:00:00")
    val endTime = format.parseDateTime("2019-01-01 12:05:00")
    val uniqueId = "123456.789"
    val channelName = "SIP/larerx9a-00000008"
    val callChannel = CallTestUtils.createCallChannel(id = None, startTime = startTime, endTime = None,
      uniqueId = uniqueId, termination = None)

    val probe = TestProbe()

    val cel = EmptyObjectProvider.emptyCel().copy(id = 1, eventTime = endTime, linkedId = uniqueId, chanName = channelName,
      uniqueId = uniqueId, eventType = "LINKEDID_END")

    val linkedIdEnd = new LinkedIdEnd
    val flowUnderTest = linkedIdEnd.flow

    val testSink = TestSink.probe[CallChannel]
    def streamUnderTest(cel: LinkedIdEndCel) = Source(List(cel)).via(flowUnderTest)
  }

  "LinkedIdEnd Flow" should "delete call channel" in new Helper {
    val expectedCallChannel = callChannel.copy(deleted = true)

    celRepository.addCallToMap(uniqueId, expectedCallChannel)

    streamUnderTest(LinkedIdEndCel(cel)).runWith(testSink).request(2).expectNext(expectedCallChannel).expectComplete()
    celRepository.repository.isEmpty shouldBe true
  }

  it should "not delete pickup call" in new Helper {
    val pickupCel = cel.copy(appName = "AppDial", cidDnid = "*8202")

    val expectedCallChannel = callChannel

    celRepository.addCallToMap(uniqueId, expectedCallChannel)

    streamUnderTest(LinkedIdEndCel(pickupCel)).runWith(testSink).request(2).expectComplete()
    celRepository.repository.isEmpty shouldBe false
    celRepository.repository(uniqueId) shouldBe expectedCallChannel
  }
}
