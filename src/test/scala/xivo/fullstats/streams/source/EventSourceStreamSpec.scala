package xivo.fullstats.streams.source

import java.sql.Connection

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import akka.stream.testkit.scaladsl.TestSink
import akka.testkit.TestKit
import org.scalatest.BeforeAndAfterAll
import xivo.fullstats.iterators.ClosingHolder
import xivo.fullstats.model._
import xivo.fullstats.testutils.{CallTestUtils, CelTestUtils, DBTest, DBUtil}
import xivo.fullstats.{EmptyObjectProvider, EventSourceStream}

class EventSourceStreamSpec extends DBTest(List("cel", "queue_log", "xc_call_channel", "xc_queue_call")) with BeforeAndAfterAll {

  implicit val system: ActorSystem = ActorSystem()
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  class Helper() {
    val startTime = format.parseDateTime("2019-01-01 12:00:00")
    val endTime = format.parseDateTime("2019-01-01 12:05:00")
    val uniqueId = "123456.789"
    val channelName = "SIP/larerx9a-00000008"

    val closingHolder = new ClosingHolder

    val callChannel = CallTestUtils.createCallChannel(id = None, startTime = startTime, endTime = None,
      emitted = false, uniqueId = uniqueId, termination = None)

    val cel = EmptyObjectProvider.emptyCel().copy(id = 1, eventTime = startTime, linkedId = uniqueId,
      uniqueId = uniqueId, eventType = "CHAN_START")

    val queueCall = CallTestUtils.createQueueCall(id = None, startTime = startTime, endTime = None,
      uniqueId = uniqueId)

    val ql: QueueLog = EmptyObjectProvider.emptyQueueLog().copy(callid = uniqueId, event = "ENTERQUEUE", time = startTime, queueName = "queue1")

    val testSink = TestSink.probe[CallEvent]
  }

  "EventSourceStreamSpec" should "emit first queue logs and then cels" in new Helper {
    val toInsertCels: List[Cel] = (for (i <- 1 to 5) yield cel.copy(id = i)).toList
    toInsertCels.foreach(CelTestUtils.insertCel)

    val toInsertQueueLogs: List[QueueLog] = (for (i <- 1 to 5) yield ql.copy(id = i)).toList
    toInsertQueueLogs.foreach(QueueLogTestUtils.insertQueueLog)

    val callEvents = new EventSourceStream(0, 0, closingHolder) {
      override def getNewDatabaseConnection: Connection = DBUtil.getConnection
    }

    Source.fromGraph(callEvents.callEventsSource)
      .runWith(testSink)
      .request(10)
      .expectNextN(toInsertQueueLogs ++ toInsertCels)
  }

  it should "get call channel start time first" in new Helper {
    CallChannel.insert(callChannel.copy(startTime = startTime))
    QueueCall.insert(queueCall.copy(startTime = startTime.plusSeconds(1)))

    val callEvents = new EventSourceStream(0, 0, closingHolder) {
      override def getNewDatabaseConnection: Connection = DBUtil.getConnection
    }


    callEvents.pendingStartTime.get shouldEqual startTime
  }

  it should "get queue call start time first" in new Helper {
    CallChannel.insert(callChannel.copy(startTime = startTime.plusSeconds(1)))
    QueueCall.insert(queueCall.copy(startTime = startTime))

    val callEvents = new EventSourceStream(0, 0, closingHolder) {
      override def getNewDatabaseConnection: Connection = DBUtil.getConnection
    }

    callEvents.pendingStartTime.get shouldEqual startTime
  }

  it should "get call channel start time only" in new Helper {
    CallChannel.insert(callChannel.copy(startTime = startTime))

    val callEvents = new EventSourceStream(0, 0, closingHolder) {
      override def getNewDatabaseConnection: Connection = DBUtil.getConnection
    }

    callEvents.pendingStartTime.get shouldEqual startTime
  }

  it should "get queue call start time only" in new Helper {
    QueueCall.insert(queueCall.copy(startTime = startTime))

    val callEvents = new EventSourceStream(0, 0, closingHolder) {
      override def getNewDatabaseConnection: Connection = DBUtil.getConnection
    }

    callEvents.pendingStartTime.get shouldEqual startTime
  }

  it should "get no start time" in new Helper {
    val callEvents = new EventSourceStream(0, 0, closingHolder) {
      override def getNewDatabaseConnection: Connection = DBUtil.getConnection
    }

    callEvents.pendingStartTime shouldEqual None
  }
}
