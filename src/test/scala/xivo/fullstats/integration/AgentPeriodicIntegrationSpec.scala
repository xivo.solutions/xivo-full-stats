package xivo.fullstats.integration

import org.joda.time.Period
import xivo.fullstats.CallEventsLoop
import xivo.fullstats.agent.StatAgentManager
import xivo.fullstats.model._
import xivo.fullstats.testutils.DBTest

class AgentPeriodicIntegrationSpec extends DBTest(List("queue_log", "stat_agent_periodic", "agent_states")) {

  "StatAgentManager" should "insert new stat_agent_periodic and agent_states based on the queue_logs" in {
    val existingPeriod = StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:00:00"), "1000", 600, 60, 0)
    StatAgentPeriodicManagerImpl.insertAll(List(existingPeriod))
    val state1000 = AgentState(format.parseDateTime("2014-01-01 08:10:00"), State.Paused)
    val state4000 = AgentState(format.parseDateTime("2014-01-01 08:10:00"), State.LoggedOff)
    AgentState.replaceState("1000", state1000)
    AgentState.replaceState("4000", state4000)
    val queueLogs = List(
      QueueLog(1, format.parseDateTime("2014-01-01 08:16:00"), "", "", "Agent/2000", "AGENTCALLBACKLOGIN", None, None, None, None, None),
      QueueLog(2, format.parseDateTime("2014-01-01 08:18:00"), "", "", "Agent/1000", "UNPAUSEALL", None, None, None, None, None),
      QueueLog(3, format.parseDateTime("2014-01-01 08:20:00"), "", "", "Agent/3000", "AGENTCALLBACKLOGOFF", None, None, None, None, None),
      QueueLog(4, format.parseDateTime("2014-01-01 08:27:00"), "", "", "Agent/2000", "WRAPUPSTART", Some("15"), None, None, None, None),
      QueueLog(5, format.parseDateTime("2014-01-01 08:27:10"), "", "", "Agent/2000", "UNPAUSEALL", None, None, None, None, None),
      QueueLog(6, format.parseDateTime("2014-01-01 08:50:00"), "", "", "Agent/1000", "AGENTCALLBACKLOGOFF", None, None, None, None, None),
      QueueLog(7, format.parseDateTime("2014-01-01 09:01:00"), "", "", "Agent/2000", "PAUSEALL", None, None, None, None, None),
      QueueLog(8, format.parseDateTime("2014-01-01 09:14:00"), "", "", "Agent/4000", "AGENTCALLBACKLOGIN", None, None, None, None, None),
      QueueLog(9, format.parseDateTime("2014-01-01 09:16:00"), "", "test", "", "ENTERQUEUE", None, None, None, None, None)
    )
    queueLogs.foreach(QueueLogTestUtils.insertQueueLog)

    val loop = new CallEventsLoop(queueLogs, List(), List(new StatAgentManager(new Period(0, 15, 0, 0))))
    loop.processNewCallEvents()

    StatAgentPeriodicTestUtils.allStatAgentPeriodic().map(_.copy(id=None)) should contain theSameElementsAs List(
      StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:00:00"), "1000", 900, 360, 0),
      StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:15:00"), "1000", 900, 180, 0),
      StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:30:00"), "1000", 900, 0, 0),
      StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:45:00"), "1000", 300, 0, 0),
      StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:15:00"), "2000", 840, 0, 10),
      StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:30:00"), "2000", 900, 0, 0),
      StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:45:00"), "2000", 900, 0, 0),
      StatAgentPeriodic(None, format.parseDateTime("2014-01-01 09:00:00"), "2000", 900, 840, 0),
      StatAgentPeriodic(None, format.parseDateTime("2014-01-01 08:15:00"), "3000", 300, 0, 0),
      StatAgentPeriodic(None, format.parseDateTime("2014-01-01 09:15:00"), "2000", 60, 60, 0),
      StatAgentPeriodic(None, format.parseDateTime("2014-01-01 09:00:00"), "4000", 60, 0, 0),
      StatAgentPeriodic(None, format.parseDateTime("2014-01-01 09:15:00"), "4000", 60, 0, 0)
    )
    AgentStateTestUtils.getStates() shouldEqual Map(
      "1000" -> AgentState(format.parseDateTime("2014-01-01 09:16:00"), State.LoggedOff),
      "2000" -> AgentState(format.parseDateTime("2014-01-01 09:16:00"), State.Paused),
      "3000" -> AgentState(format.parseDateTime("2014-01-01 09:16:00"), State.LoggedOff),
      "4000" -> AgentState(format.parseDateTime("2014-01-01 09:16:00"), State.LoggedOn)
    )

  }

}
