package xivo.fullstats.testutils

import java.io.File
import java.sql.{Connection, DriverManager}
import java.util.Properties

import anorm.SQL
import org.dbunit.database.DatabaseConnection
import org.dbunit.dataset.DefaultDataSet
import org.dbunit.dataset.csv.{CsvDataSet, CsvDataSetWriter}
import org.dbunit.dataset.xml.{FlatXmlDataSet, FlatXmlProducer}
import org.dbunit.operation.DatabaseOperation
import org.slf4j.LoggerFactory
import org.xml.sax.InputSource

import scala.xml.XML


object DBUtil {
  val logger = LoggerFactory.getLogger(getClass)

  val HOST = "localhost"
  val DB_NAME = "asterisktest"
  val USER = "asterisk"
  val PASSWORD = "asterisk"
  val connection = getConnection
  var setupDone = false

  val sqlScripts = List("full_stats_create_tables.sql")

  def getConnection: Connection = {
    Class.forName("org.postgresql.Driver")
    val uri = "jdbc:postgresql://" + HOST + "/" + DB_NAME
    val props = new Properties()
    props.setProperty("user", USER)
    props.setProperty("password", PASSWORD)
    DriverManager.getConnection(uri, props)
  }

  def setupDB(filename: String)(implicit connection: Connection) = {

    if(!setupDone) {
      createDababaseFromLiquibase()

      val dataset = new FlatXmlDataSet(new FlatXmlProducer(new InputSource(getClass.getClassLoader.getResourceAsStream(filename))))
      for (table <- dataset.getTableNames) {
        val createTable = scala.io.Source.fromInputStream(getClass.getClassLoader.getResourceAsStream(s"$table.sql")).mkString
        SQL(createTable).execute
      }
      val dbunitConnection: DatabaseConnection = new DatabaseConnection(connection)
      DatabaseOperation.CLEAN_INSERT.execute(dbunitConnection, dataset)
      setupDone = true
    }
  }

  def createDababaseFromLiquibase()(implicit connection: Connection): Unit = {

    def removeTableToFixInvalidCreationScripts():Unit = {
      logger.info("Removing table to fix invalid creation scripts")
      SQL("DROP TABLE IF EXISTS queue_threshold_time CASCADE;").execute
      SQL("DROP TYPE IF EXISTS callback_status CASCADE;").execute
      SQL("DROP TABLE IF EXISTS callback_ticket CASCADE;").execute
      SQL("DROP TABLE IF EXISTS transfers CASCADE;").execute
    }

    removeTableToFixInvalidCreationScripts()

    val changelog = XML.load(getClass.getClassLoader.getResourceAsStream("db.changelog.xml"))

    val changesets = changelog \\ "databaseChangeLog" \\ "changeSet" \\ "sqlFile"

    changesets.foreach{ n =>
      val file  = (n \ "@path").text
      logger.info(s"processing : $file")
      val createTable = scala.io.Source.fromInputStream(getClass.getClassLoader.getResourceAsStream(s"$file")).mkString
      SQL(createTable).execute
    }


  }

  def cleanTables(tables: List[String])(implicit connection: Connection) {
    val st = connection.createStatement()
    st.executeUpdate(s"TRUNCATE ${tables.mkString(",")} CASCADE")
  }

  def createCsv(tableName: String) = {
    var table = new DatabaseConnection(connection).createTable(tableName)
    val dataset = new DefaultDataSet(table)
    CsvDataSetWriter.write(dataset, new File(getClass.getClassLoader.getResource("CSV/").getFile))
  }

  def insertFromCsv() = {
    val dataset = new CsvDataSet(new File(getClass.getClassLoader.getResource("CSV/").getFile))
    DatabaseOperation.CLEAN_INSERT.execute(new DatabaseConnection(connection), dataset)
  }

}