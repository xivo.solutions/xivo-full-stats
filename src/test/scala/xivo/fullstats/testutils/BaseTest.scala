package xivo.fullstats.testutils

import org.scalatest.{BeforeAndAfterEach, Matchers, WordSpec}

abstract class BaseTest extends WordSpec with Matchers with BeforeAndAfterEach
