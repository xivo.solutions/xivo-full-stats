package xivo.fullstats.model

import org.scalatest.{FlatSpec, Matchers}

class SqlUtilsSpec extends FlatSpec with Matchers {

 "SqlUtils" should "create an IN clause" in {
   SqlUtils.createInClauseOrTrue("callid", List("c1", "c2", "c3")) shouldEqual "callid IN ('c1','c2','c3')"
 }

  it should "return TRUE" in {
    SqlUtils.createInClauseOrTrue("callid", List()) shouldEqual "TRUE"
  }

  it should "return FALSE" in {
    SqlUtils.createInClauseOrFalse("callid", List()) shouldEqual "FALSE"
  }

  it should "return FALSE fast" in {
    SqlUtils.createInClauseOrFalseFast("callid", List()) shouldEqual "FALSE"
  }
}

