package xivo.fullstats.model

import java.sql.Connection
import java.util.Date

import anorm.SqlParser._
import anorm._
import org.joda.time.DateTime
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.testutils.DBTest

class CallDataSpec extends DBTest(List("attached_data", "hold_periods", "call_data", "agentfeatures")) {

  "The CallData singleton" should "insert CallDatas in the database" in {
    val cd1 = CallData(None, "1234456.789", Some("1000"), CallDirection.Internal, format.parseDateTime("2014-05-21 12:14:35"), Some(format.parseDateTime("2014-05-21 12:14:40")),
      Some(format.parseDateTime("2014-05-21 12:18:35")), Some("answer"), Some(10), false, Some("1300"), Some(CallDirection.Outgoing), None, Some("2000"), Some("2001"), Some("SIP/agtdfzt"))
    val cd2 = CallData(None, "1234456.777", Some("1001"), CallDirection.Outgoing, format.parseDateTime("2014-05-21 12:14:46"), Some(format.parseDateTime("2014-05-21 12:14:50")),
      Some(format.parseDateTime("2014-05-21 12:19:35")), Some("answer"), Some(5), true, Some("1301"), Some(CallDirection.Internal))

    List(cd1, cd2).foreach(CallData.insertOrUpdate)

    CallDataTestUtils.allCallData.map(_.copy(id = None)) shouldEqual List(cd1, cd2)
  }

  it should "not crash Anorm with null columns" in {
    val cd1 = CallData(None, "123456", None, CallDirection.Internal, new DateTime(), None, None, None, None, false, None, None)

    CallData.insertOrUpdate(cd1)
  }

  it should "insert CallData with their associated AttachedData and HoldPeriods" in {
    val cd1 = CallData(None, "1234456.789", Some("1000"), CallDirection.Internal, format.parseDateTime("2014-05-21 12:14:35"), Some(format.parseDateTime("2014-05-21 12:14:40")),
      Some(format.parseDateTime("2014-05-21 12:18:35")), Some("answer"), Some(10), false, Some("1300"), Some(CallDirection.Outgoing))
    cd1.addAttachedData(AttachedData("key1", "value1"))
    cd1.addAttachedData(AttachedData("key2", "value2"))
    cd1.addHoldPeriod(HoldPeriod(format.parseDateTime("2014-05-21 12:15:00"), Some(format.parseDateTime("2014-05-21 12:15:15")), cd1.uniqueId))
    cd1.addHoldPeriod(HoldPeriod(format.parseDateTime("2014-05-21 12:16:00"), Some(format.parseDateTime("2014-05-21 12:16:15")), cd1.uniqueId))

    CallData.insertOrUpdate(cd1)

    CallDataTestUtils.allCallData.map(cd => cd.uniqueId) shouldEqual List("1234456.789")
    val idCallData = CallDataTestUtils.allCallData.head.id.get
    CallDataTestUtils.attachedDataForCall(idCallData) shouldEqual List(AttachedData("key1", "value1"), AttachedData("key2", "value2"))
    CallDataTestUtils.holdPeriodsForCall(idCallData) shouldEqual List(
      HoldPeriod(format.parseDateTime("2014-05-21 12:15:00"), Some(format.parseDateTime("2014-05-21 12:15:15")), cd1.uniqueId),
      HoldPeriod(format.parseDateTime("2014-05-21 12:16:00"), Some(format.parseDateTime("2014-05-21 12:16:15")), cd1.uniqueId))
  }

  it should "not remove attached data with xivo recording expiration key" in {
    val cd1: CallData = CallData(None, "1234456.789", Some("1000"), CallDirection.Internal, format.parseDateTime("2014-05-21 12:14:35"), Some(format.parseDateTime("2014-05-21 12:14:40")),
      Some(format.parseDateTime("2014-05-21 12:18:35")), Some("answer"), Some(10), false, Some("1300"), Some(CallDirection.Outgoing))
    val attachedDataKey1 = AttachedData("key1", "value1")
    val attachedDataKey2 = AttachedData("key2", "value2")
    val attachedDataPurgeKey = AttachedData(AttachedData.PurgeKey, "value1")
    cd1.addAttachedData(attachedDataKey1)
    cd1.addAttachedData(attachedDataPurgeKey)
    cd1.addAttachedData(attachedDataKey2)
    cd1.addHoldPeriod(HoldPeriod(format.parseDateTime("2014-05-21 12:15:00"), Some(format.parseDateTime("2014-05-21 12:15:15")), cd1.uniqueId))
    cd1.addHoldPeriod(HoldPeriod(format.parseDateTime("2014-05-21 12:16:00"), Some(format.parseDateTime("2014-05-21 12:16:15")), cd1.uniqueId))

    CallData.insertOrUpdate(cd1)
    val idCallData = CallDataTestUtils.allCallData.head.id.get

    CallData.update(cd1.copy(id = Some(idCallData), attachedData = List(attachedDataKey1, attachedDataKey2)))

    CallDataTestUtils.allCallData.map(cd => cd.uniqueId) shouldEqual List("1234456.789")
    CallDataTestUtils.attachedDataForCall(idCallData) shouldEqual List(attachedDataPurgeKey, attachedDataKey1, attachedDataKey2)


  }

  it should "override src num & dst num with attached data" in {
    val cd1 = CallData(None, "1234456.789", Some("1000"), CallDirection.Internal, format.parseDateTime("2014-05-21 12:14:35"), Some(format.parseDateTime("2014-05-21 12:14:40")),
      Some(format.parseDateTime("2014-05-21 12:18:35")), Some("answer"), Some(10), false, Some("1300"), Some(CallDirection.Outgoing))
    cd1.addAttachedData(AttachedData("STAT_SRC_NUM", "1234"))
    cd1.addAttachedData(AttachedData("STAT_DST_NUM", "4321"))

    cd1.srcNum shouldBe Some("1234")
    cd1.dstNum shouldBe Some("4321")
  }

  it should "store stat app name" in {
    val cd1 = CallData(None, "1234456.789", Some("1000"), CallDirection.Internal, format.parseDateTime("2014-05-21 12:14:35"), Some(format.parseDateTime("2014-05-21 12:14:40")),
      Some(format.parseDateTime("2014-05-21 12:18:35")), Some("answer"), Some(10), false, Some("1300"), Some(CallDirection.Outgoing))
    cd1.addAttachedData(AttachedData("STAT_APP_NAME", "Originate"))

    cd1.statAppName shouldBe Some("Originate")
  }

  it should "retrieve callids of pending calls" in {
    val cd1 = CallData(None, "1234456.789", Some("1000"), CallDirection.Internal, format.parseDateTime("2014-05-21 12:14:35"), Some(format.parseDateTime("2014-05-21 12:14:40")),
      None, Some("answer"), Some(10), false, Some("1300"), Some(CallDirection.Outgoing))
    val cd2 = CallData(None, "1234456.777", Some("1001"), CallDirection.Outgoing, format.parseDateTime("2014-05-21 12:14:46"), Some(format.parseDateTime("2014-05-21 12:14:50")),
      Some(format.parseDateTime("2014-05-21 12:19:35")), Some("answer"), Some(5), true, Some("1301"), Some(CallDirection.Internal))
    val cd3 = CallData(None, "1234456.788", Some("1000"), CallDirection.Internal, format.parseDateTime("2014-05-21 12:15:35"), Some(format.parseDateTime("2014-05-21 12:15:40")),
      None, Some("answer"), Some(10), false, Some("1300"), Some(CallDirection.Outgoing))
    List(cd1, cd2, cd3).foreach(CallData.insertOrUpdate)

    CallData.getPendingCallIds().sorted shouldEqual List("1234456.789", "1234456.788").sorted
  }

  it should "delete calls by callid" in {
    val cd1 = CallData.insertOrUpdate(CallData(None, "1234456.789", Some("1000"), CallDirection.Internal, format.parseDateTime("2014-05-21 12:14:35"), Some(format.parseDateTime("2014-05-21 12:14:40")),
      None, Some("answer"), Some(10), false, Some("1300"), Some(CallDirection.Outgoing)))
    CallElement.insert(CallElement(None, cd1.id.get, new DateTime, None, None, "SIP/agbhjfzs", None))
    val cd2 = CallData.insertOrUpdate(CallData(None, "1234456.777", Some("1001"), CallDirection.Outgoing, format.parseDateTime("2014-05-21 12:14:46"), Some(format.parseDateTime("2014-05-21 12:14:50")),
      Some(format.parseDateTime("2014-05-21 12:19:35")), Some("answer"), Some(5), true, Some("1301"), Some(CallDirection.Internal)))
    var cd3 = CallData(None, "1234456.788", Some("1000"), CallDirection.Internal, format.parseDateTime("2014-05-21 12:15:35"), Some(format.parseDateTime("2014-05-21 12:15:40")),
      None, Some("answer"), Some(10), false, Some("1300"), Some(CallDirection.Outgoing))
    cd3.addAttachedData(AttachedData("key", "value"))
    cd3.addHoldPeriod(HoldPeriod(format.parseDateTime("2014-05-21 12:15:50"), Some(format.parseDateTime("2014-05-21 12:15:50")), cd2.uniqueId))
    cd3 = CallData.insertOrUpdate(cd3)

    CallData.deleteByCallid(List(cd1.uniqueId, cd3.uniqueId))

    CallDataTestUtils.allCallData.map(_.uniqueId) shouldEqual List("1234456.777")
    CallElementTestUtils.allCallElements shouldEqual List()
  }

  it should "delete nothing if an empty list is provided" in {
    CallData.insertOrUpdate(CallData(None, "1234456.789", Some("1000"), CallDirection.Internal, format.parseDateTime("2014-05-21 12:14:35"), Some(format.parseDateTime("2014-05-21 12:14:40")),
      None, Some("answer"), Some(10), false, Some("1300"), Some(CallDirection.Outgoing)))

    CallData.deleteByCallid(List())

    CallDataTestUtils.allCallData.map(_.uniqueId) shouldEqual List("1234456.789")
  }

  it should "delete stale pending calls (older then 1 day)" in {
    val cd0 = CallData(None, "1234456.600", Some("1000"), CallDirection.Internal, format.parseDateTime("2014-05-21 11:14:35"), Some(format.parseDateTime("2014-05-21 11:14:40")),
      Some(format.parseDateTime("2014-05-21 11:14:50")), Some("answer"), Some(10), false, Some("1300"), Some(CallDirection.Outgoing))
    val cd1 = CallData(None, "1234456.789", Some("1000"), CallDirection.Internal, format.parseDateTime("2014-05-21 12:14:35"), Some(format.parseDateTime("2014-05-21 12:14:40")),
      None, Some("answer"), Some(10), false, Some("1300"), Some(CallDirection.Outgoing))
    val cd2 = CallData(None, "1234456.777", Some("1001"), CallDirection.Outgoing, format.parseDateTime("2014-05-21 15:14:46"), Some(format.parseDateTime("2014-05-21 15:14:50")),
      None, Some("answer"), Some(5), true, Some("1301"), Some(CallDirection.Internal))
    val cd3 = CallData(None, "1234456.788", Some("1000"), CallDirection.Internal, format.parseDateTime("2014-05-22 14:00:35"), Some(format.parseDateTime("2014-05-22 14:00:40")),
      None, Some("answer"), Some(10), false, Some("1300"), Some(CallDirection.Outgoing))
    cd1.addAttachedData(AttachedData("key", "value"))
    cd1.addHoldPeriod(HoldPeriod(format.parseDateTime("2014-05-21 12:15:50"), Some(format.parseDateTime("2014-05-21 12:15:50")), cd1.uniqueId))
    List(cd0, cd1, cd2, cd3).foreach(CallData.insertOrUpdate)

    CallData.deleteStalePendingCalls()

    CallDataTestUtils.allCallData.map(_.uniqueId) shouldEqual List("1234456.600", "1234456.777", "1234456.788")
  }

  it should "update a CallData if the id exists" in {
    val origCd = CallData.insert(CallData(None, "12345.789", None, CallDirection.Internal, format.parseDateTime("2015-01-01 08:00:00"), None, None, None, None,
      false, None, None, attachedData = List(AttachedData("key1", "value1")), holdPeriods = List(HoldPeriod(format.parseDateTime("2015-01-01 08:00:15"), None, "12345.789"))))
    origCd.id.isDefined shouldBe true
    val newCd = origCd.copy(srcNum = Some("2000"), endTime = Some(format.parseDateTime("2015-01-01 08:01:00")), srcInterface = Some("SIP/ajdkz"),
      attachedData = List(AttachedData("key1", "value1"), AttachedData("key2", "value2")),
      holdPeriods = List(HoldPeriod(format.parseDateTime("2015-01-01 08:00:15"), Some(format.parseDateTime("2015-01-01 08:00:30")), "12345.789")))
    CallData.insertOrUpdate(newCd)

    CallDataTestUtils.allCallData shouldEqual List(newCd)
  }

  "The CallData class" should "append attached data to the end of the list" in {
    val cd = EmptyObjectProvider.emptyCallData()
    cd.addAttachedData(AttachedData("key1", "value1"))
    cd.addAttachedData(AttachedData("key2", "value2"))

    cd shouldEqual cd.copy(attachedData = List(AttachedData("key1", "value1"), AttachedData("key2", "value2")))
  }

  it should "return empty string if no channel2_uniqueid in extra data" in {
    val key = "channel2_uniqueid"
    val extraData = "{\"bridge1_id\":\"51eb0750-71a3-497c-8be8-8517d580a7fe\",\"channel2_name\":\"SIP/gb2hv32m-0000003b\",\"bridge2_id\":\"5da5667b-1fa2-48f3-b41b-526bc37bdf0b\",\"transfer_target_channel_uniqueid\":\"1503911760.136\",\"transferee_channel_name\":\"SIP/ga3te1ei-00000039\",\"transferee_channel_uniqueid\":\"1503911749.129\",\"transfer_target_channel_name\":\"SIP/187w60v8-0000003c\"}"
    CallData.getCallExtraData(key, extraData) shouldEqual None
  }

  it should "return value from key 'channel2_uniqueid' in extra data" in {
    val key = "channel2_uniqueid"
    val extraData = "{\"bridge1_id\":\"51eb0750-71a3-497c-8be8-8517d580a7fe\",\"channel2_uniqueid\":\"1442841277.34\",\"channel2_name\":\"SIP/gb2hv32m-0000003b\",\"bridge2_id\":\"5da5667b-1fa2-48f3-b41b-526bc37bdf0b\",\"transfer_target_channel_uniqueid\":\"1503911760.136\",\"transferee_channel_name\":\"SIP/ga3te1ei-00000039\",\"transferee_channel_uniqueid\":\"1503911749.129\",\"transfer_target_channel_name\":\"SIP/187w60v8-0000003c\"}"
    CallData.getCallExtraData(key, extraData).get shouldEqual "1442841277.34"
  }

  it should "translate agent number from agent ID" in {
    val cd1 = CallData(None, "1234456.789", Some("0123121010"), CallDirection.Internal, format.parseDateTime("2014-05-21 12:14:35"), Some(format.parseDateTime("2014-05-21 12:14:40")),
      Some(format.parseDateTime("2014-05-21 12:18:35")), Some("answer"), Some(10), false, Some("1300"), Some(CallDirection.Outgoing), None,
      srcAgent = Some("2000"), dstAgent = None, Some("SIP/agtdfzt"), agentId = Some(7))

    CallDataTestUtils.populateAgentFeatures(7, 1, 1000, "User", "One")
    List(cd1).foreach(CallData.insertOrUpdate)


    CallDataTestUtils.allCallData.map(_.dstAgent) shouldEqual List(Some("1000"))
  }

  it should "return agent number from position if destination number is agent's number" in {
    val cd1 = CallData(None, "1234456.789", Some("1004"), CallDirection.Internal, format.parseDateTime("2014-05-21 12:14:35"), Some(format.parseDateTime("2014-05-21 12:14:40")),
      Some(format.parseDateTime("2014-05-21 12:18:35")), Some("answer"), Some(10), false, srcNum = Some("external-41304"), Some(CallDirection.Outgoing),
      srcAgent = None, dstAgent = None, srcInterface = Some("SIP/agtdfzt"), agentId = None, cidNum = Some("1004"))

    val openedPosition = AgentPosition("1000", "1004", None, new DateTime(format.parseDateTime("2014-01-02 08:00:00")), None)
    AgentPosition.insertOrUpdate(openedPosition)

    CallDataTestUtils.populateAgentFeatures(7, 1, 1000, "User", "One")
    List(cd1).foreach(CallData.insertOrUpdate)

    AgentPosition.agentForNumAndTime("1004", format.parseDateTime("2014-05-21 12:14:35")) shouldEqual Some("1000")
    CallDataTestUtils.allCallData.map(_.dstAgent) shouldEqual List(Some("1000"))
  }

  it should "return agent number from position if dstAgent and agentId is empty" in {
    val cd1 = CallData(None, "1234456.789", Some("0123121010"), CallDirection.Internal, format.parseDateTime("2014-05-21 12:14:35"), Some(format.parseDateTime("2014-05-21 12:14:40")),
      Some(format.parseDateTime("2014-05-21 12:18:35")), Some("answer"), Some(10), false, srcNum = Some("external-41304"), Some(CallDirection.Outgoing),
      srcAgent = None, dstAgent = None, srcInterface = Some("SIP/agtdfzt"), agentId = None, cidNum = Some("1004"))

    val openedPosition = AgentPosition("1000", "1004", None, new DateTime(format.parseDateTime("2014-01-02 08:00:00")), None)
    AgentPosition.insertOrUpdate(openedPosition)

    CallDataTestUtils.populateAgentFeatures(7, 1, 1000, "User", "One")
    List(cd1).foreach(CallData.insertOrUpdate)

    AgentPosition.agentForNumAndTime("1004", format.parseDateTime("2014-05-21 12:14:35")) shouldEqual Some("1000")
    CallDataTestUtils.allCallData.map(_.dstAgent) shouldEqual List(Some("1000"))
  }

  it should "set transferTime if callData contains at least one transfer"in {
    val cd = CallData(None, "1234456.789", Some("1000"), CallDirection.Internal, format.parseDateTime("2018-11-30 12:00:00"), Some(format.parseDateTime("2018-11-30 12:14:40")),
      Some(format.parseDateTime("2018-11-30 12:18:35")), Some("answer"), Some(10), transfered = true, Some("1300"), Some(CallDirection.Outgoing), None, Some("2000"), Some("2001"), Some("SIP/agtdfzt"))

    val transfer = Transfers("1234456.789", "4566789.123")
    val transferTime = format.parseDateTime("2018-11-30 12:16:00")

    cd.addTransfers(transferTime, transfer)
    cd.transferTime shouldEqual Some(transferTime)
    cd.transfers shouldEqual List(transfer)
  }

  it should "keep first transferTime in callData if new transfer comes"in {
    val transfer = Transfers("1234456.789", "4566789.123")
    val transferTime = format.parseDateTime("2018-11-30 12:15:00")

    val cd = CallData(None, "1234456.789", Some("1000"), CallDirection.Internal, format.parseDateTime("2018-11-30 12:00:00"), Some(format.parseDateTime("2018-11-30 12:14:40")),
      Some(format.parseDateTime("2018-11-30 12:18:35")), Some("answer"), Some(10), transfered = true, Some("1300"), Some(CallDirection.Outgoing), Some(transferTime), Some("2000"), Some("2001"), Some("SIP/agtdfzt"),
      transfers = List(transfer))

    val newTransfer = Transfers("4566789.123", "6453321.789")
    val newTransferTime = format.parseDateTime("2018-11-30 12:16:00")

    cd.addTransfers(newTransferTime, newTransfer)
    cd.transferTime shouldEqual Some(transferTime)
    cd.transfers should contain only(newTransfer, transfer)
  }

}

object CallDataTestUtils {
  def callData(implicit c: Connection) = get[Option[Int]]("id") ~
    get[String]("uniqueid") ~
    get[Option[String]]("dst_num") ~
    get[Date]("start_time") ~
    get[Option[Date]]("answer_time") ~
    get[Option[Date]]("end_time") ~
    get[Option[String]]("status") ~
    get[Option[Int]]("ring_duration_on_answer") ~
    get[Boolean]("transfered") ~
    get[String]("call_direction") ~
    get[Option[String]]("src_num") ~
    get[Option[String]]("transfer_direction") ~
    get[Option[String]]("src_agent") ~
    get[Option[String]]("dst_agent") ~
    get[Option[String]]("src_interface") map {
    case id ~ uId ~ dstNum ~ start ~ answer ~ end ~ status ~ ringDur ~ transfered ~ callDir ~ srcNum ~ transferDir ~ srcAgent ~ dstAgent ~ srcInterface =>
      CallData(id, uId, dstNum, CallDirection.withName(callDir), new DateTime(start), answer.map(new DateTime(_)), end.map(new DateTime(_)), status, ringDur, transfered,
        srcNum, transferDir.map(CallDirection.withName), None, srcAgent, dstAgent, srcInterface, attachedDataForCall(id.get), holdPeriodsForCall(id.get), transfersDataForCall(id.get))
  }

  def allCallData(implicit c: Connection): List[CallData] = SQL("SELECT id, uniqueid, dst_num, start_time, answer_time, end_time, status::varchar, ring_duration_on_answer, transfered," +
    "call_direction::varchar, src_num, transfer_direction::varchar, src_agent, dst_agent, src_interface FROM call_data").as(callData *)

  val attachedData = get[String]("key") ~ get[String]("value") map {
    case key ~ value => AttachedData(key, value)
  }

  def attachedDataForCall(idCallData: Int)(implicit c: Connection) = SQL("SELECT key, value FROM attached_data WHERE id_call_data = {id} ORDER BY id ASC").on('id -> idCallData).as(attachedData *)

  val holdPeriod = get[Date]("start") ~ get[Option[Date]]("end") ~ get[String]("linkedid") map {
    case start ~ end ~ linkedid => HoldPeriod(new DateTime(start), end.map(new DateTime(_)), linkedid)
  }

  def holdPeriodsForCall(idCallData: Int)(implicit c: Connection) = SQL("SELECT start, \"end\", linkedid FROM hold_periods WHERE linkedid = (SELECT uniqueid FROM call_data WHERE id = {id})" +
    " ORDER BY start ASC").on('id -> idCallData).as(holdPeriod *)

  val transfer = get[String]("callidfrom") ~ get[String]("callidto") map {
    case callidfrom ~ callidto => Transfers(callidfrom, callidto)
  }

  def transfersDataForCall(idCallData: Int)(implicit c: Connection) = SQL("SELECT callidfrom, callidto FROM transfers WHERE callidfrom = (SELECT uniqueid FROM call_data WHERE id = {id})")
    .on('id -> idCallData).as(transfer *)

  def populateAgentFeatures(id: Int, numgroup: Int, number: Int, firstname: String, lastname: String)(implicit c: Connection) =
    SQL("""INSERT INTO agentfeatures(id, numgroup, number, firstname, lastname) VALUES ({id}, {numgroup}, {number}, {firstname}, {lastname})""")
      .on('id -> id, 'numgroup -> numgroup, 'number -> number, 'firstname -> firstname, 'lastname -> lastname)
      .executeInsert()

}
