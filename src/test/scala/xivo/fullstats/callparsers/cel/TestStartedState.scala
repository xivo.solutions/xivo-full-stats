package xivo.fullstats.callparsers.cel

import xivo.fullstats.model.{AttachedData, CallDirection}

class TestStartedState extends TestState {

  "The Started state" should "return an Answered state on ANWSER" in {
    cel.eventType = "ANSWER"
    cel.appName = "AppDial"
    val started = new Started(callData, cel)

    started.processCel(cel).isInstanceOf[Answered] shouldBe true
  }

  "The Started state" should "return an Answered state when MeetMe answers" in {
    cel.eventType = "ANSWER"
    cel.appName = "MeetMe"
    val started = new Started(callData, cel)

    started.processCel(cel).isInstanceOf[Answered] shouldBe true
  }

  "The Started state" should "return an Answered state when AppDial2 answers and in originate" in {
    cel.eventType = "ANSWER"
    cel.appName = "AppDial2"
    callData.statAppName = Some("Originate")
    val started = new Started(callData, cel)

    started.processCel(cel).isInstanceOf[Answered] shouldBe true
  }

  "The Started state" should "return an Answered state when MeetMe starts" in {
    cel.eventType = "APP_START"
    cel.appName = "MeetMe"
    val started = new Started(callData, cel)

    started.processCel(cel).isInstanceOf[Answered] shouldBe true
  }

  "The Started state" should "stay started when AppDial2 answers and not in originate" in {
    cel.eventType = "ANSWER"
    cel.appName = "AppDial2"
    callData.statAppName = None
    val started = new Started(callData, cel)

    started.processCel(cel).isInstanceOf[Started] shouldBe true
  }

  it should "return a HangedUp state on LINKEDID_END" in {
    cel.eventType = "LINKEDID_END"
    val started = new Started(callData, cel)

    started.processCel(cel).isInstanceOf[HangedUp] shouldBe true
  }

  it should "set the call direction to 'outgoing' on XIVO_OUTCALL" in {
    cel.eventType = "XIVO_OUTCALL"
    val started = new Started(callData, cel)

    started.processCel(cel) should be theSameInstanceAs started
    callData.callDirection should equal(CallDirection.Outgoing)
  }

  it should "set the call direction to 'incoming' in XIVO_INCALL" in {
    cel.eventType = "XIVO_INCALL"
    val started = new Started(callData, cel)

    started.processCel(cel) should be theSameInstanceAs started
    callData.callDirection should equal(CallDirection.Incoming)
  }

  it should "return a Ringing state on APP_START with appname Dial" in {
    cel.eventType = "APP_START"
    cel.appName = "Dial"
    val started = new Started(callData, cel)

    started.processCel(cel).isInstanceOf[Ringing] shouldBe true
  }

  it should "return a Ringing state on APP_START with appname Queue" in {
    cel.eventType = "APP_START"
    cel.appName = "Queue"
    val started = new Started(callData, cel)

    started.processCel(cel).isInstanceOf[Ringing] shouldBe true
  }

  it should "set dstNum on APP_START with unfilled dstNum" in {
    cel.eventType = "APP_START"
    cel.appName = "Dial"
    cel.cidNum = "12345"
    val started = new Started(callData, cel)

    started.processCel(cel).result.dstNum should equal(Some("12345"))
  }

  it should "set the dstNum on ANSWER if it was not set before" in {
    cel.eventType = "ANSWER"
    cel.appName = "Queue"
    cel.cidNum = "1200"
    val started = new Started(callData, cel)

    started.processCel(cel)

    callData.dstNum should equal(Some("1200"))
  }

  it should "not set the dstNum on APP_START if it was set before" in {
    cel.eventType = "APP_START"
    cel.appName = "Dial"
    cel.cidAni = "1200"
    callData.dstNum = Some("3000")
    val started = new Started(callData, cel)

    started.processCel(cel)

    callData.dstNum should equal(Some("3000"))
  }

  it should "set the dstNum and return a StartedAcd state on OUTCALL_ACD" in {
    cel.eventType = "OUTCALL_ACD"
    cel.cidNum = "014578"
    val started = new Started(callData, cel)

    started.processCel(cel).isInstanceOf[StartedAcd] shouldBe true
    started.getResult.dstNum shouldEqual Some("014578")
  }

  it should "add (XIVO_CUID, 12345) to attached data" in {
    cel.eventType = "XIVO_CUID"
    cel.appData = "XIVO_CUID,12345"
    val started = new Started(callData, cel)

    started.processCel(cel).isInstanceOf[Started] shouldBe true
    started.getResult.attachedData.size shouldEqual 1
    val data = started.getResult.attachedData.head
    data.key shouldEqual "XIVO_CUID"
    data.value shouldEqual "12345"
  }
}
