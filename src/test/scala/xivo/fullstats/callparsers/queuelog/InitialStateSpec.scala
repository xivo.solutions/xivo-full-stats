package xivo.fullstats.callparsers.queuelog

import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model._
import xivo.fullstats.testutils.{CelTestUtils, DBTest}

class InitialStateSpec extends DBTest(List("cel")) {

  var state: InitialState = _
  val callId              = "123456.789"

  override def beforeEach(): Unit = {
    super.beforeEach()
    CelTestUtils.insertCel(
      EmptyObjectProvider.emptyCel().copy(uniqueId = callId, linkedId = callId)
    )
    state = new InitialState(EmptyObjectProvider.emptyCallOnQueue())
  }

  "The InitialState" should "set the time, queue and callid and return Queued on ENTERQUEUE" in {
    val date = format.parseDateTime("2013-01-01 08:00:00")
    val ql = QueueLog(
      1,
      date,
      callId,
      "queue01",
      "NONE",
      "ENTERQUEUE",
      None,
      None,
      None,
      None,
      None
    )

    val res = state.processEvent(ql)

    res.getClass shouldEqual classOf[Queued]
    res.getResult shouldEqual CallOnQueue(
      None,
      Some(callId),
      date,
      0,
      None,
      None,
      None,
      "queue01",
      None
    )
    state.result.isLinkedIdFound shouldBe true
  }

  it should "set the time, queue, status and callid and return Finished on CLOSED" in {
    val date = format.parseDateTime("2013-01-01 08:00:00")
    val ql = QueueLog(
      1,
      date,
      callId,
      "queue01",
      "NONE",
      "CLOSED",
      None,
      None,
      None,
      None,
      None
    )

    val res = state.processEvent(ql)

    res.getClass shouldEqual classOf[Finished]
    res.getResult shouldEqual CallOnQueue(
      None,
      Some(callId),
      date,
      0,
      None,
      Some(date),
      Some(CallExitType.Closed),
      "queue01",
      None
    )
    state.result.isLinkedIdFound shouldBe true
  }

  it should "set the time, queue, status and callid and return Finished on FULL" in {
    val date = format.parseDateTime("2013-01-01 08:00:00")
    val ql = QueueLog(
      1,
      date,
      callId,
      "queue01",
      "NONE",
      "FULL",
      None,
      None,
      None,
      None,
      None
    )

    val res = state.processEvent(ql)

    res.getClass shouldEqual classOf[Finished]
    res.getResult shouldEqual CallOnQueue(
      None,
      Some(callId),
      date,
      0,
      None,
      Some(date),
      Some(CallExitType.Full),
      "queue01",
      None
    )
    state.result.isLinkedIdFound shouldBe true
  }

  it should "set the time, queue, status and callid and return Finished on JOINEMPTY" in {
    val date = format.parseDateTime("2013-01-01 08:00:00")
    val ql = QueueLog(
      1,
      date,
      callId,
      "queue01",
      "NONE",
      "JOINEMPTY",
      None,
      None,
      None,
      None,
      None
    )

    val res = state.processEvent(ql)

    res.getClass shouldEqual classOf[Finished]
    res.getResult shouldEqual CallOnQueue(
      None,
      Some(callId),
      date,
      0,
      None,
      Some(date),
      Some(CallExitType.JoinEmpty),
      "queue01",
      None
    )
    state.result.isLinkedIdFound shouldBe true
  }

  it should "set the time, queue, status and callid and return Finished on DIVERT_CA_RATIO" in {
    val date = format.parseDateTime("2013-01-01 08:00:00")
    val ql = QueueLog(
      1,
      date,
      callId,
      "queue01",
      "NONE",
      "DIVERT_CA_RATIO",
      None,
      None,
      None,
      None,
      None
    )

    val res = state.processEvent(ql)

    res.getClass shouldEqual classOf[Finished]
    res.getResult shouldEqual CallOnQueue(
      None,
      Some(callId),
      date,
      0,
      None,
      Some(date),
      Some(CallExitType.DivertCaRatio),
      "queue01",
      None
    )
    state.result.isLinkedIdFound shouldBe true
  }

  it should "set the time, queue, status and callid and return Finished on DIVERT_HOLDTIME" in {
    val date = format.parseDateTime("2013-01-01 08:00:00")
    val ql = QueueLog(
      1,
      date,
      callId,
      "queue01",
      "NONE",
      "DIVERT_HOLDTIME",
      None,
      None,
      None,
      None,
      None
    )

    val res = state.processEvent(ql)

    res.getClass shouldEqual classOf[Finished]
    res.getResult shouldEqual CallOnQueue(
      None,
      Some(callId),
      date,
      0,
      None,
      Some(date),
      Some(CallExitType.DivertWaitTime),
      "queue01",
      None
    )
    state.result.isLinkedIdFound shouldBe true
  }

  it should "not fail if there is no CEL associated with the queue_log" in {
    val date = format.parseDateTime("2013-01-01 08:00:00")
    val ql = QueueLog(
      1,
      date,
      callId + "3",
      "queue01",
      "NONE",
      "ENTERQUEUE",
      None,
      None,
      None,
      None,
      None
    )

    val res = state.processEvent(ql)

    res.result.callid shouldEqual Some(callId + "3")
    res.result.isLinkedIdFound shouldBe false
  }

  it should "do nothing (and not throw exception) when encountering a missed consultation call (queue call transferred to another queue that was rejected)" in {
    val ql = QueueLog(
      1,
      format.parseDateTime("2022-02-09 14:05:00"),
      callId,
      "queue01",
      "NONE",
      "COMPLETEAGENT",
      None,
      None,
      Some("240"),
      None,
      None
    )
    state.processEvent(ql) shouldBe theSameInstanceAs(state)
  }

}
