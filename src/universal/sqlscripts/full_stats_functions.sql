DROP FUNCTION IF EXISTS round_to_x_seconds(timestamp without time zone, integer);
CREATE OR REPLACE FUNCTION round_to_x_seconds(the_date timestamp without time zone, seconds integer) RETURNS timestamp without time zone AS
$$
DECLARE
  delta INTERVAL;
  new_date TIMESTAMP;
BEGIN
  delta := the_date - the_date::timestamp with time zone at time zone 'UTC';
  new_date = the_date::timestamp(0);

  RETURN new_date - (((CAST(extract(epoch FROM new_date + delta) AS INTEGER) + 3*24*3600) % seconds) * INTERVAL '1 second');
END
$$
LANGUAGE plpgsql;


DROP FUNCTION IF EXISTS insert_stat_queue_specific(varchar, varchar);
CREATE OR REPLACE FUNCTION insert_stat_queue_specific(start_date VARCHAR, end_date VARCHAR) RETURNS void AS
$$
DECLARE
    query VARCHAR;
BEGIN
    query := 'SELECT 1 AS place_holder FROM insert_stat_queue_specific_with_cfg(
                $1,
                $2,
                (SELECT coalesce(max(seconds), 15) FROM queue_specific_time_period WHERE name=''t1''),
                (SELECT coalesce(max(seconds), 20) FROM queue_specific_time_period WHERE name=''t2'')
              );';
    EXECUTE query USING start_date, end_date;
END
$$
LANGUAGE plpgsql;


-- fonction remplissant la table stat_queue_specific
DROP FUNCTION IF EXISTS insert_stat_queue_specific_with_cfg(varchar, varchar, integer, integer);
CREATE OR REPLACE FUNCTION insert_stat_queue_specific_with_cfg(start_date VARCHAR, end_date VARCHAR, time1 integer, time2 integer) RETURNS void AS
$$
DECLARE
    query VARCHAR;
BEGIN
    query := 'INSERT INTO stat_queue_specific("time", queue_ref, dst_num, nb_offered, answer_less_t1, answer_btw_t1_t2, nb_abandoned, sum_resp_delay, abandoned_btw_t1_t2, abandoned_more_t2, communication_time, wrapup_time, hold_time) (
    SELECT thetime, queue_ref, dst_num, t1.nb_offered, t3.answer_less_t1, t3.answer_btw_t1_t2, t5.nb_abandoned, t1.sum_resp_delay, t5.abandoned_btw_t1_t2, t5.abandoned_more_t2, EXTRACT(epoch FROM t3.communication_time), t2.wrapup_time, EXTRACT(epoch FROM t4.hold_time) FROM
        (SELECT $1::timestamp thetime, cq.queue_ref, c.dst_num,
                sum(CASE WHEN cq.status IN (''answered'', ''abandoned'', ''leaveempty'', ''timeout'', ''exit_with_key'') OR cq.status IS NULL THEN 1 ELSE 0 END) AS nb_offered,
                sum(CASE WHEN cq.status = ''answered'' THEN EXTRACT(epoch FROM (cq.answer_time - cq.queue_time)) ELSE 0 END) AS sum_resp_delay
            FROM call_on_queue cq INNER JOIN call_data c ON cq.callid = c.uniqueid
            WHERE cq.queue_time >= $1::timestamp AND cq.queue_time < $2::timestamp AND (cq.hangup_time >= $1::timestamp OR cq.hangup_time IS NULL)
            GROUP BY thetime, cq.queue_ref, c.dst_num) t1 NATURAL FULL JOIN
        (SELECT $1::timestamp AS thetime, q.queuename AS queue_ref, c.dst_num, sum(CAST(q.data1 AS INTEGER)) AS wrapup_time
            FROM queue_log q JOIN call_data c ON q.callid = c.uniqueid
            WHERE q.event = ''WRAPUPSTART'' AND q.time >= $1 AND q.time < $2
            GROUP BY thetime, queue_ref, c.dst_num) t2 NATURAL FULL JOIN
        (SELECT $1::timestamp thetime, cq.queue_ref, c.dst_num,
                sum(least($2::timestamp, cq.hangup_time) - greatest($1::timestamp, cq.answer_time)) AS communication_time,
                sum(CASE WHEN cq.answer_time >= $1::timestamp AND EXTRACT(epoch FROM (cq.answer_time - cq.queue_time)) <= $3 THEN 1 ELSE 0 END) AS answer_less_t1,
                sum(CASE WHEN cq.answer_time >= $1::timestamp AND EXTRACT(epoch FROM (cq.answer_time - cq.queue_time)) <= $4 AND EXTRACT(epoch FROM (cq.answer_time - cq.queue_time)) > $3 THEN 1 ELSE 0 END) AS answer_btw_t1_t2
            FROM call_on_queue cq INNER JOIN call_data c ON cq.callid = c.uniqueid
            WHERE cq.answer_time < $2::timestamp AND (cq.hangup_time >= $1::timestamp OR cq.hangup_time IS NULL)
            GROUP BY thetime, cq.queue_ref, c.dst_num) t3 NATURAL FULL JOIN
        (SELECT $1::timestamp thetime, cq.queue_ref, c.dst_num, sum(least($2::timestamp, hp.end) - greatest($1::timestamp, hp.start)) AS hold_time
            FROM call_on_queue cq INNER JOIN call_data c ON cq.callid = c.uniqueid INNER JOIN hold_periods hp ON hp.linkedid = c.uniqueid
            WHERE cq.answer_time < $2::timestamp AND (cq.hangup_time >= $1::timestamp OR cq.hangup_time IS NULL) AND hp.start < $2::timestamp AND (hp.end IS NULL OR hp.end >= $1::timestamp)
            GROUP BY thetime, cq.queue_ref, c.dst_num) t4 NATURAL FULL JOIN
        (SELECT $1::timestamp thetime, cq.queue_ref, c.dst_num,
                count(*) AS nb_abandoned,
                sum(CASE WHEN EXTRACT(epoch FROM (cq.hangup_time - cq.queue_time)) > $3 AND EXTRACT(epoch FROM (cq.hangup_time - cq.queue_time)) <= $4 THEN 1 ELSE 0 END) AS abandoned_btw_t1_t2,
                sum(CASE WHEN cq.status = ''abandoned'' AND EXTRACT(epoch FROM (cq.hangup_time - cq.queue_time)) > $4 THEN 1 ELSE 0 END) AS abandoned_more_t2
            FROM call_on_queue cq INNER JOIN call_data c ON cq.callid = c.uniqueid
            WHERE cq.hangup_time >= $1::timestamp AND cq.hangup_time < $2::timestamp AND cq.status = ''abandoned''
            GROUP BY thetime, queue_ref, c.dst_num) t5
    )';
    EXECUTE query USING start_date, end_date, time1, time2;
END
$$
LANGUAGE plpgsql;


-- fonction calculant le temps de sonnerie des agents (regroupés par quart d'heure)
DROP FUNCTION IF EXISTS ringing_time(varchar, varchar);
CREATE OR REPLACE FUNCTION ringing_time(start_date varchar, end_date varchar) RETURNS TABLE(thetime timestamp without time zone, agent_num varchar, total_ring_time integer)
AS
$$
DECLARE
    query VARCHAR;
BEGIN
    query := 'SELECT rtime, ragent_num, coalesce(ring_time_answered, 0) + coalesce(ring_time_ignored, 0) AS total_ring_time FROM
    (SELECT $1::timestamp AS rtime, cq.agent_num AS ragent_num, CAST(sum(call.ring_duration_on_answer) AS INTEGER) AS ring_time_answered
        FROM call_on_queue cq INNER JOIN call_data call ON call.uniqueid = cq.callid
        WHERE cq.status = ''answered'' AND cq.answer_time >= $1::timestamp AND cq.answer_time < $2::timestamp AND call.call_direction != ''outgoing''
        GROUP BY rtime, ragent_num) t1 NATURAL FULL JOIN
    (SELECT $1::timestamp AS rtime, regexp_replace(agent, ''Agent/'', '''') AS ragent_num, CAST(sum(CAST(data1 AS integer)/1000) AS INTEGER) AS ring_time_ignored
        FROM queue_log WHERE event = ''RINGNOANSWER'' AND time >= $1 AND time < $2
        GROUP BY rtime, ragent_num) t2';
    RETURN QUERY
         EXECUTE query USING start_date, end_date;
END
$$
LANGUAGE plpgsql;

-- fonction remplissant la table stat_agent_specific
DROP FUNCTION IF EXISTS insert_stat_agent_specific(varchar, varchar);
CREATE OR REPLACE FUNCTION insert_stat_agent_specific(start_date VARCHAR, end_date VARCHAR) RETURNS void AS
$$
DECLARE
    query VARCHAR;
BEGIN
    query := 'INSERT INTO stat_agent_specific ("time", agent_num, nb_offered, nb_answered, conversation_time, ringing_time, nb_outgoing_calls, conversation_time_outgoing_calls, nb_received_internal_calls, conversation_time_received_internal_calls, nb_transfered_intern, nb_transfered_extern, nb_emitted_internal_calls, conversation_time_emitted_internal_calls, nb_incoming_calls, conversation_time_incoming_calls, hold_time) (
    SELECT $1::timestamp, agent_num, t1.nb_offered, t1.nb_answered, EXTRACT(epoch FROM t2.conversation_time), t3.total_ring_time, t4.nb_outgoing_calls, EXTRACT(epoch FROM t7.conversation_time_outgoing_calls), t5.nb_received_internal_calls, EXTRACT(epoch FROM t8.conversation_time_received_internal_calls), t6.nb_transfered_intern, t6.nb_transfered_extern, t4.nb_emitted_internal_calls, EXTRACT(epoch FROM t7.conversation_time_emitted_internal_calls), t5.nb_incoming_calls, EXTRACT(epoch FROM t8.conversation_time_incoming_calls), EXTRACT(epoch FROM t9.hold_time) FROM
        (SELECT regexp_replace(agent, ''Agent/'', '''') AS agent_num, count(*) AS nb_offered, sum(CASE WHEN event = ''CONNECT'' THEN 1 ELSE 0 END) AS nb_answered
            FROM queue_log ql JOIN call_data cd ON ql.callid = cd.uniqueid WHERE ql.event IN(''CONNECT'', ''RINGNOANSWER'') AND ql.time >= $1 AND ql.time < $2 and cd.call_direction != ''outgoing''
            GROUP BY agent_num) t1 NATURAL FULL JOIN
        (SELECT cq.agent_num, sum(least($2::timestamp, cq.hangup_time) - greatest($1::timestamp, cq.answer_time)) AS conversation_time
            FROM call_on_queue cq JOIN call_data cd ON cq.callid = cd.uniqueid
            WHERE cq.answer_time < $2::timestamp AND (cq.hangup_time >= $1::timestamp OR cq.hangup_time IS NULL) AND cd.call_direction != ''outgoing''
            GROUP BY agent_num) t2 NATURAL FULL JOIN
        (SELECT agent_num, total_ring_time FROM ringing_time($1, $2)) t3 NATURAL FULL JOIN
        (SELECT c.src_agent AS agent_num, sum(CASE WHEN c.call_direction = ''outgoing'' THEN 1 ELSE 0 END) AS nb_outgoing_calls,
                             sum(CASE WHEN c.call_direction = ''internal'' AND c.dst_num NOT LIKE ''*%'' THEN 1 ELSE 0 END) AS nb_emitted_internal_calls
            FROM call_data c WHERE c.start_time < $2::timestamp AND c.start_time >= $1::timestamp AND c.src_agent IS NOT NULL
            GROUP BY agent_num) t4 NATURAL FULL JOIN
        (SELECT c.dst_agent AS agent_num, sum(CASE WHEN c.call_direction = ''internal'' THEN 1 ELSE 0 END) AS nb_received_internal_calls,
                             sum(CASE WHEN c.call_direction = ''incoming'' THEN 1 ELSE 0 END) AS nb_incoming_calls
            FROM call_data c WHERE c.start_time < $2::timestamp AND c.start_time >= $1::timestamp AND c.dst_agent IS NOT NULL
            GROUP BY agent_num) t5 NATURAL FULL JOIN
        (SELECT regexp_replace(q.agent, ''Agent/'', '''') AS agent_num, sum(CASE WHEN c.transfer_direction = ''internal'' THEN 1 ELSE 0 END) AS nb_transfered_intern,
                                                                        sum(CASE WHEN c.transfer_direction = ''outgoing'' THEN 1 ELSE 0 END) AS nb_transfered_extern
            FROM queue_log q INNER JOIN call_data c ON q.callid = c.uniqueid
            WHERE q.event = ''TRANSFER'' AND q.time >= $1 AND q.time < $2
            GROUP BY agent_num) t6 NATURAL FULL JOIN
        (SELECT c.src_agent AS agent_num,
                sum(CASE WHEN c.call_direction = ''outgoing'' THEN least($2::timestamp, c.end_time) - greatest($1::timestamp, c.answer_time) ELSE INTERVAL ''0'' END) AS conversation_time_outgoing_calls,
                sum(CASE WHEN c.call_direction = ''internal'' AND c.dst_num NOT LIKE ''*%'' THEN least($2::timestamp, c.end_time) - greatest($1::timestamp, c.answer_time) ELSE INTERVAL ''0'' END) AS conversation_time_emitted_internal_calls
            FROM call_data c WHERE c.answer_time < $2::timestamp AND (c.end_time >= $1::timestamp OR c.end_time IS NULL) AND c.src_agent IS NOT NULL
            GROUP BY agent_num) t7 NATURAL FULL JOIN
        (SELECT c.dst_agent AS agent_num,
                sum(CASE WHEN c.call_direction = ''internal'' THEN least($2::timestamp, c.end_time) - greatest($1::timestamp, c.answer_time) ELSE INTERVAL ''0'' END) AS conversation_time_received_internal_calls,
                sum(CASE WHEN c.call_direction = ''incoming'' THEN least($2::timestamp, c.end_time) - greatest($1::timestamp, c.answer_time) ELSE INTERVAL ''0'' END) AS conversation_time_incoming_calls
            FROM call_data c WHERE c.answer_time < $2::timestamp AND (c.end_time >= $1::timestamp OR c.end_time IS NULL) AND c.dst_agent IS NOT NULL
            GROUP BY agent_num) t8 NATURAL FULL JOIN
        (SELECT cq.agent_num, sum(least($2::timestamp, hp.end) - greatest($1::timestamp, hp.start)) AS hold_time
            FROM call_on_queue cq INNER JOIN hold_periods hp ON hp.linkedid = cq.callid
            WHERE cq.answer_time < $2::timestamp AND (cq.hangup_time >= $1::timestamp OR cq.hangup_time IS NULL) AND hp.start < $2::timestamp AND (hp.end IS NULL OR hp.end >= $1::timestamp)
            GROUP BY cq.agent_num) t9
    )';
    EXECUTE query USING start_date, end_date;
END
$$
LANGUAGE plpgsql;


DROP FUNCTION IF EXISTS insert_stat_agent_queue_specific(varchar, varchar);
CREATE OR REPLACE FUNCTION insert_stat_agent_queue_specific(start_date VARCHAR, end_date varchar) RETURNS void AS
$$
DECLARE
    query VARCHAR;
BEGIN
    query := 'INSERT INTO stat_agent_queue_specific ("time", agent_num, queue_ref, dst_num, nb_answered_calls, communication_time, wrapup_time, hold_time) (
    SELECT $1::timestamp, agent_num, queue_ref, dst_num, COALESCE(t2.nb_answered_calls, 0), EXTRACT(epoch FROM t1.communication_time), t3.wrapup_time, EXTRACT(epoch FROM t4.hold_time) FROM
        (SELECT cq.agent_num AS agent_num, cq.queue_ref AS queue_ref, c.dst_num AS dst_num,
                sum(least($2::timestamp, cq.hangup_time) - greatest($1::timestamp, cq.answer_time)) AS communication_time
            FROM call_on_queue cq INNER JOIN call_data c ON cq.callid = c.uniqueid
            WHERE cq.status = ''answered'' AND cq.answer_time < $2::timestamp AND (cq.hangup_time >= $1::timestamp OR cq.hangup_time IS NULL)
            GROUP BY agent_num, queue_ref, dst_num) t1 NATURAL FULL JOIN
        (SELECT cq.agent_num AS agent_num, cq.queue_ref AS queue_ref, c.dst_num AS dst_num, count(*) AS nb_answered_calls
            FROM call_on_queue cq INNER JOIN call_data c ON cq.callid = c.uniqueid
            WHERE cq.status = ''answered'' AND cq.answer_time >= $1::timestamp AND cq.answer_time < $2::timestamp AND (cq.hangup_time >= $1::timestamp OR cq.hangup_time IS NULL)
            GROUP BY agent_num, queue_ref, dst_num) t2 NATURAL FULL JOIN
        (SELECT regexp_replace(q.agent, ''Agent/'', '''') AS agent_num, q.queuename AS queue_ref, c.dst_num AS dst_num, sum(CAST(q.data1 AS INTEGER)) AS wrapup_time
            FROM queue_log q JOIN call_data c ON q.callid = c.uniqueid WHERE event = ''WRAPUPSTART'' AND time >= $1 AND time < $2
            GROUP BY agent_num, queue_ref, dst_num) t3 NATURAL FULL JOIN
        (SELECT cq.agent_num, cq.queue_ref, cd.dst_num, sum(least($2::timestamp, hp.end) - greatest($1::timestamp, hp.start)) AS hold_time
            FROM call_on_queue cq INNER JOIN call_data cd ON cq.callid = cd.uniqueid INNER JOIN hold_periods hp ON hp.linkedid = cq.callid
            WHERE cq.answer_time < $2::timestamp AND (cq.hangup_time >= $1::timestamp OR cq.hangup_time IS NULL) AND hp.start < $2::timestamp AND (hp.end IS NULL OR hp.end >= $1::timestamp)
            GROUP BY cq.agent_num, queue_ref, dst_num) t4
    )';
    EXECUTE query USING start_date, end_date;
END
$$
LANGUAGE plpgsql;