DROP TYPE IF EXISTS agent_state_type CASCADE;
CREATE TYPE agent_state_type AS ENUM('logged_on', 'logged_off', 'paused');

DROP TABLE IF EXISTS agent_states;
CREATE TABLE agent_states (
    agent VARCHAR(128),
    time TIMESTAMP WITHOUT TIME ZONE,
    state agent_state_type
);

DROP TABLE IF EXISTS "stat_agent_periodic" CASCADE;
CREATE TABLE "stat_agent_periodic" (
 "id" SERIAL PRIMARY KEY,
 "time" timestamp NOT NULL,
 "agent" VARCHAR(128),
 "login_time" INTERVAL NOT NULL DEFAULT '00:00:00',
 "pause_time" INTERVAL NOT NULL DEFAULT '00:00:00',
 "wrapup_time" INTERVAL NOT NULL DEFAULT '00:00:00'
);
CREATE INDEX "stat_agent_periodic__idx__time" ON stat_agent_periodic(time);


DROP TYPE IF EXISTS "call_exit_type" CASCADE;
CREATE TYPE "call_exit_type" AS ENUM (
  'full',
  'closed',
  'joinempty',
  'leaveempty',
  'divert_ca_ratio',
  'divert_waittime',
  'answered',
  'abandoned',
  'timeout',
  'exit_with_key'
);


DROP TABLE IF EXISTS last_queue_log_id;
CREATE TABLE last_queue_log_id (
    id INTEGER
);


DROP TABLE IF EXISTS "call_on_queue" CASCADE;
CREATE TABLE "call_on_queue" (
 "id" SERIAL PRIMARY KEY,
 "callid" VARCHAR(32),
 "queue_time" timestamp NOT NULL,
 "total_ring_seconds" INTEGER NOT NULL DEFAULT 0,
 "answer_time" timestamp,
 "hangup_time" timestamp ,
 "status" call_exit_type,
 "queue_ref" VARCHAR(128),
 "agent_num" VARCHAR(128)
);
CREATE INDEX "call_on_queue__idx__callid" ON call_on_queue(callid);
CREATE INDEX "call_on_queue__idx__queue_time" ON call_on_queue(queue_time);
CREATE INDEX "call_on_queue__idx__answer_time" ON call_on_queue(answer_time);


DROP TABLE IF EXISTS "stat_queue_periodic" CASCADE;
CREATE TABLE "stat_queue_periodic" (
 "id" SERIAL PRIMARY KEY,
 "time" timestamp NOT NULL,
 "queue" VARCHAR(128) NOT NULL,
 "answered" INTEGER NOT NULL DEFAULT 0,
 "abandoned" INTEGER NOT NULL DEFAULT 0,
 "total" INTEGER NOT NULL DEFAULT 0,
 "full" INTEGER NOT NULL DEFAULT 0,
 "closed" INTEGER NOT NULL DEFAULT 0,
 "joinempty" INTEGER NOT NULL DEFAULT 0,
 "leaveempty" INTEGER NOT NULL DEFAULT 0,
 "divert_ca_ratio" INTEGER NOT NULL DEFAULT 0,
 "divert_waittime" INTEGER NOT NULL DEFAULT 0,
 "timeout" INTEGER NOT NULL DEFAULT 0,
 "exit_with_key" INTEGER NOT NULL DEFAULT 0
);
CREATE INDEX "stat_queue_periodic__idx__time" ON stat_queue_periodic(time);

/**********************************/
/*   tables pour xivo-call-data   */
/*********************************/

DROP TYPE IF EXISTS status_type CASCADE;
CREATE TYPE status_type AS ENUM ('answer', 'busy', 'cancel', 'failed');

DROP TYPE IF EXISTS call_direction_type CASCADE;
CREATE TYPE call_direction_type AS ENUM('incoming', 'outgoing', 'internal');

DROP TABLE IF EXISTS "call_data" CASCADE;
CREATE TABLE call_data (
    id SERIAL PRIMARY KEY,
    uniqueid VARCHAR(150) NOT NULL,
    dst_num VARCHAR(80),
    start_time TIMESTAMP WITHOUT TIME ZONE,
    answer_time TIMESTAMP WITHOUT TIME ZONE,
    end_time TIMESTAMP WITHOUT TIME ZONE,
    status status_type,
    ring_duration_on_answer INTEGER,
    transfered BOOLEAN,
    call_direction call_direction_type,
    src_num VARCHAR(80),
    transfer_direction call_direction_type,
    src_agent VARCHAR(128),
    dst_agent VARCHAR(128),
    src_interface VARCHAR(255)
);
CREATE INDEX call_data__idx__start_time ON call_data(start_time);
CREATE INDEX call_data__idx__end_time ON call_data(end_time);
CREATE INDEX call_data__idx__uniqueid ON call_data(uniqueid);

DROP TABLE IF EXISTS call_element CASCADE;
CREATE TABLE call_element (
    id SERIAL PRIMARY KEY,
    call_data_id INTEGER REFERENCES call_data(id) NOT NULL,
    start_time TIMESTAMP WITHOUT TIME ZONE,
    answer_time TIMESTAMP WITHOUT TIME ZONE,
    end_time TIMESTAMP WITHOUT TIME ZONE,
    interface VARCHAR(255),
    agent VARCHAR(128)
);
CREATE INDEX call_element__idx__call_data_id ON call_element(call_data_id);
CREATE INDEX call_element__idx__interface ON call_element(interface);

DROP TABLE IF EXISTS attached_data;
CREATE TABLE attached_data (
    id SERIAL PRIMARY KEY,
    id_call_data INTEGER REFERENCES call_data(id),
    key VARCHAR(128),
    value VARCHAR(256)
);
CREATE INDEX attached_data__idx__id_call_data ON attached_data(id_call_data);
CREATE INDEX attached_data__idx__key_value ON attached_data (key, value);

DROP TABLE IF EXISTS last_cel_id;
CREATE TABLE last_cel_id (
    id INTEGER
);

DROP TABLE IF EXISTS hold_periods;
CREATE TABLE hold_periods (
    id SERIAL PRIMARY KEY,
    linkedid VARCHAR(150),
    start TIMESTAMP WITHOUT TIME ZONE,
    "end" TIMESTAMP WITHOUT TIME ZONE
);
CREATE INDEX hold_periods__idx__linkedid ON hold_periods(linkedid);

/************************************************************
 *          compteurs par file d'attente / numéro          *
 ************************************************************/

-- table contenant les résultats des statistiques spécifiques par file d'attente
DROP TABLE IF EXISTS stat_queue_specific;
CREATE TABLE stat_queue_specific (
    "time" TIMESTAMP WITHOUT TIME ZONE,
    queue_ref VARCHAR(50),
    dst_num VARCHAR(80),
    nb_offered INTEGER,
    nb_abandoned INTEGER,
    sum_resp_delay INTEGER,
    answer_less_t1 INTEGER,
    abandoned_btw_t1_t2 INTEGER,
    answer_btw_t1_t2 INTEGER,
    abandoned_more_t2 INTEGER,
    communication_time INTEGER,
    hold_time INTEGER,
    wrapup_time INTEGER
);
CREATE INDEX "stat_queue_specific__idx__time" ON stat_queue_specific(time);

DROP TABLE IF EXISTS queue_specific_time_period;
CREATE TABLE queue_specific_time_period (
    name VARCHAR(50) PRIMARY KEY,
    seconds INTEGER
);

INSERT INTO queue_specific_time_period(name, seconds) VALUES ('t1', 15),('t2', 20);


/************************************************************
 *                   compteurs par agent                    *
 ************************************************************/

-- table contenant les résultats des statistiques spécifiques par agent
DROP TABLE IF EXISTS stat_agent_specific;
CREATE TABLE stat_agent_specific (
    "time" TIMESTAMP WITHOUT TIME ZONE,
    agent_num VARCHAR(50),
    nb_offered INTEGER,
    nb_answered INTEGER,
    conversation_time INTEGER,
    ringing_time INTEGER,
    nb_outgoing_calls INTEGER,
    conversation_time_outgoing_calls INTEGER,
    hold_time INTEGER,
    nb_received_internal_calls INTEGER,
    conversation_time_received_internal_calls INTEGER,
    nb_transfered_intern INTEGER,
    nb_transfered_extern INTEGER,
    nb_emitted_internal_calls INTEGER,
    conversation_time_emitted_internal_calls INTEGER,
    nb_incoming_calls INTEGER,
    conversation_time_incoming_calls INTEGER
);
CREATE INDEX "stat_agent_specific__idx__time" ON stat_agent_specific(time);

-- table intermédiaire pour les appels sortants par agent
DROP TABLE IF EXISTS agent_position;
CREATE TABLE agent_position (
    agent_num VARCHAR(50) NOT NULL,
    line_number VARCHAR(10),
    start_time TIMESTAMP WITHOUT TIME ZONE,
    end_time TIMESTAMP WITHOUT TIME ZONE,
    sda VARCHAR(40)
);
CREATE INDEX agent_position__idx__start_time ON agent_position (start_time);


/*******************************************************
 *   compteurs par file - numéro entrant et par agent  *
 *******************************************************/
-- table contenant les résultats des statistiques spécifiques par file - numéro entrant et par agent
DROP TABLE IF EXISTS stat_agent_queue_specific;
CREATE TABLE stat_agent_queue_specific (
    "time" TIMESTAMP WITHOUT TIME ZONE,
    agent_num VARCHAR(50),
    queue_ref VARCHAR(50),
    nb_answered_calls INTEGER,
    communication_time INTEGER,
    hold_time INTEGER,
    wrapup_time INTEGER,
    dst_num VARCHAR
);
CREATE INDEX "stat_agent_queue_specific__idx__time" ON stat_agent_queue_specific(time);


GRANT SELECT ON ALL TABLES IN SCHEMA public TO stats;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA public TO stats;
