CREATE INDEX call_channel__idx__original_call_id ON xc_call_channel(original_call_id);
CREATE INDEX call_channel__idx__user_id ON xc_call_channel(user_id);
CREATE INDEX call_channel__idx__agent_id ON xc_call_channel(agent_id);
