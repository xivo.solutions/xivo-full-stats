ALTER TABLE xc_call_channel RENAME asterisk_id TO unique_id;
ALTER TABLE xc_queue_call RENAME asterisk_id TO unique_id;

DROP INDEX call_channel__idx__asterisk_id;
CREATE INDEX call_channel__idx__queue_call_id ON xc_call_channel(queue_call_id);