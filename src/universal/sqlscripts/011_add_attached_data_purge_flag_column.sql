DO $$
    BEGIN
        BEGIN
            ALTER TABLE attached_data ADD COLUMN purged BOOLEAN NOT NULL DEFAULT FALSE;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column purged already exists in attached_data.';
        END;
    END;
$$;