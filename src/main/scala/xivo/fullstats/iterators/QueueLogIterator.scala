package xivo.fullstats.iterators

import java.sql.{PreparedStatement, Connection}

import org.joda.time.DateTime
import xivo.fullstats.model.QueueLog

class QueueLogIterator(startId: Int, maxId: Int, additionalCids: List[String], override val connection: Connection) extends GenericCallEventIterator[QueueLog](connection) {

  private  def requestAdditionnalCids(cids: List[String]): String = {
    var idVars = ""
    for (i <- 1 to cids.size)
      idVars += s"?,"
    if(idVars.length > 0)
      idVars = idVars.substring(0, idVars.length - 1)
    s"callid IN ( $idVars )"
  }

  override def next(): QueueLog = QueueLog(
    rs.getInt("id"),
    new DateTime(rs.getTimestamp("time")),
    rs.getString("callid"),
    rs.getString("queuename"),
    rs.getString("agent"),
    rs.getString("event"),
    Option(rs.getString("data1")),
    Option(rs.getString("data2")),
    Option(rs.getString("data3")),
    Option(rs.getString("data4")),
    Option(rs.getString("data5")))

  override def createStatement(): PreparedStatement = {
    var query = s"SELECT id, to_timestamp(time, 'YYYY-MM-DD HH24:MI:SS.US') AS time, callid, queuename, agent, event, data1, data2, data3, data4, data5" +
      s" FROM queue_log WHERE id >= $startId AND id <= $maxId"
    if(additionalCids.size > 0)
      query += s" OR ${requestAdditionnalCids(additionalCids)}"
    query +=  " ORDER BY id ASC"

    val statement = connection.prepareStatement(query)
    for(i <- 1 to additionalCids.size)
      statement.setString(i, additionalCids(i-1))
    statement
  }
}
