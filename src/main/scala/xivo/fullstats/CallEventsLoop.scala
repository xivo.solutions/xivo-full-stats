package xivo.fullstats

import java.sql.Connection

import org.slf4j.LoggerFactory
import xivo.fullstats.model._

class CallEventsLoop(eventList: Iterable[CallEvent],
                     celParsers: List[EventParser[Cel]],
                     queueLogParsers: List[EventParser[QueueLog]])(implicit c: Connection) {

  val logger = LoggerFactory.getLogger(getClass)

  def processNewCallEvents(): Unit = {
    logger.info("Starting processing of tables cel and queue_log")
    for(e <- eventList) {
      e match {
        case c: Cel => celParsers.foreach(_.parseEvent(c))
        case q: QueueLog => queueLogParsers.foreach(_.parseEvent(q))
      }
    }
    (celParsers ++ queueLogParsers).foreach(_.saveState())
    logger.info("queue_log and cel processing finished")
  }
}
