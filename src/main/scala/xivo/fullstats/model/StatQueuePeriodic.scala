package xivo.fullstats.model

import java.sql.Connection
import java.util.Date

import anorm.SqlParser._
import anorm.{SQL, ~}
import org.joda.time.DateTime

case class StatQueuePeriodic(id: Option[Int], time: DateTime, queue: String, var answered: Int, var abandoned: Int, var total: Int, var full: Int,
                             var closed: Int, var joinEmpty: Int, var leaveEmpty: Int, var divertCaRatio: Int, var divertWaitTime: Int,
                             var timeout: Int, var exitWithKey: Int) {
  def isEmpty = answered + abandoned + total + full + closed + joinEmpty + leaveEmpty + divertCaRatio + divertWaitTime + timeout + exitWithKey == 0
}

object StatQueuePeriodic {
  def apply(time: DateTime, queue: String): StatQueuePeriodic = StatQueuePeriodic(None, time, queue, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
}

trait StatQueuePeriodicManager {
  def lastForQueue(queueName: String)(implicit c: Connection): Option[StatQueuePeriodic]
  def insert(stats: Iterable[StatQueuePeriodic])(implicit c: Connection): Unit
  def insertOrUpdate(stat: StatQueuePeriodic)(implicit c: Connection): Unit
  def insert(stat: StatQueuePeriodic)(implicit c: Connection): Unit
  def update(stat: StatQueuePeriodic)(implicit c: Connection): Unit
}

object StatQueuePeriodicManagerImpl extends StatQueuePeriodicManager{
  private val simple = get[Option[Int]]("id") ~
    get[Date]("time") ~
    get[String]("queue") ~
    get[Int]("answered") ~
    get[Int]("abandoned") ~
    get[Int]("total") ~
    get[Int]("full") ~
    get[Int]("closed") ~
    get[Int]("joinempty") ~
    get[Int]("leaveempty") ~
    get[Int]("divert_ca_ratio") ~
    get[Int]("divert_waittime") ~
    get[Int]("timeout") ~
    get[Int]("exit_with_key") map {
    case id ~ time ~ queue ~ answered ~ abandoned ~ total ~ full ~ closed ~ joinempty ~ leaveempty
      ~ divertcaratio ~ divertcawaittime ~ timeout ~ exitWithKey =>
      StatQueuePeriodic(id, new DateTime(time), queue, answered, abandoned, total, full, closed, joinempty, leaveempty,
        divertcaratio, divertcawaittime, timeout, exitWithKey)
  }

  override def lastForQueue(queueName: String)(implicit c: Connection): Option[StatQueuePeriodic] = SQL("SELECT id, time, queue, answered, abandoned, total," +
    "\"full\", closed, joinempty, leaveempty, divert_ca_ratio, divert_waittime, timeout, exit_with_key FROM stat_queue_periodic WHERE queue = {queue}" +
    " ORDER BY time DESC LIMIT 1").on('queue -> queueName).as(simple *).headOption

  override def insert(stats: Iterable[StatQueuePeriodic])(implicit c: Connection): Unit = {
    c.setAutoCommit(false)
    stats.foreach(insertOrUpdate)
    c.commit()
    c.setAutoCommit(true)
  }

  override def insertOrUpdate(stat: StatQueuePeriodic)(implicit c: Connection): Unit = {
    if(stat.id.isDefined) update(stat)
    else insert(stat)
  }

  override def insert(stat: StatQueuePeriodic)(implicit c: Connection): Unit = {
    SQL("INSERT INTO stat_queue_periodic(time, queue, answered, abandoned, total, \"full\", closed, joinempty, leaveempty, divert_ca_ratio, divert_waittime, timeout, exit_with_key)" +
      " VALUES ({time}, {queue}, {answered}, {abandoned}, {total}, {full}, {closed}, {joinEmpty}, {leaveEmpty}, {divertCa}, {divertWait}, {timeout}, {exitWithKey})")
    .on('time -> stat.time.toDate, 'queue -> stat.queue, 'answered -> stat.answered, 'abandoned -> stat.abandoned, 'total -> stat.total, 'full -> stat.full,
        'closed -> stat.closed, 'joinEmpty -> stat.joinEmpty, 'leaveEmpty -> stat.leaveEmpty, 'divertCa -> stat.divertCaRatio, 'divertWait -> stat.divertWaitTime,
        'timeout -> stat.timeout, 'exitWithKey -> stat.exitWithKey).executeUpdate()
  }

  override def update(stat: StatQueuePeriodic)(implicit c: Connection): Unit = {
    SQL("UPDATE stat_queue_periodic SET answered={answered}, abandoned={abandoned}, total={total}, \"full\"={full}, closed={closed}, joinempty={joinEmpty}, leaveempty={leaveEmpty}," +
      "divert_ca_ratio={divertCa}, divert_waittime={divertWait}, timeout={timeout}, exit_with_key={exitWithKey} WHERE id = {id}").on('answered -> stat.answered, 'abandoned -> stat.abandoned,
        'total -> stat.total, 'full -> stat.full, 'closed -> stat.closed, 'joinEmpty -> stat.joinEmpty, 'leaveEmpty -> stat.leaveEmpty, 'divertCa -> stat.divertCaRatio,
        'divertWait -> stat.divertWaitTime, 'timeout -> stat.timeout, 'exitWithKey -> stat.exitWithKey, 'id -> stat.id).executeUpdate()
  }
}
