package xivo.fullstats.model

import java.sql.Connection
import anorm._
import org.joda.time.DateTime

case class CallElement(id: Option[Long],
                       callDataId: Long,
                       startTime: DateTime,
                       answerTime: Option[DateTime],
                       endTime: Option[DateTime],
                       interface: String,
                       agent: Option[String])

object CallElement {
  def update(element: CallElement)(implicit c: Connection) = SQL("UPDATE call_element SET start_time={start}, answer_time={answer}, " +
    "end_time={end}, interface={interface}, agent={agent}, call_data_id={cdId} WHERE id={id}").on('cdId -> element.callDataId,
      'start -> element.startTime.toDate, 'answer -> element.answerTime.map(_.toDate), 'end -> element.endTime.map(_.toDate),
      'interface -> element.interface, 'agent -> element.agent, 'id -> element.id).executeUpdate()

  def insert(element: CallElement)(implicit c: Connection): CallElement = {
    val id = SQL("INSERT INTO call_element(call_data_id, start_time, answer_time, end_time, interface, agent) VALUES" +
      "({cdId}, {start}, {answer}, {end}, {interface}, {agent})").on('cdId -> element.callDataId, 'start -> element.startTime.toDate,
      'answer -> element.answerTime.map(_.toDate), 'end -> element.endTime.map(_.toDate), 'interface -> element.interface, 'agent -> element.agent)
    .executeInsert()
    element.copy(id=id)
  }
}
