package xivo.fullstats.model

import java.sql.Connection
import java.util.Date

import anorm.SqlParser._
import anorm.{SQL, _}
import anorm.Column.nonNull
import org.joda.time.DateTime
import org.slf4j.LoggerFactory
import xivo.fullstats.model.CallScope.CallScope
import xivo.fullstats.model.CallTermination.CallTermination
import xivo.fullstats.model.CallType.CallType
import xivo.fullstats.model.CelAppName.AppName

object CallChannel {
  def columnToEnum[T <: Enumeration](enum: T): Column[T#Value] = {
    nonNull { (value, meta) =>
      val MetaDataItem(e, _, _) = meta
      value match {
        case s: String =>
          enum.values.find(t => t.toString == s) match {
            case Some(t) => Right(t)
            case None => Left(TypeDoesNotMatch(s"Unable to convert $value: ${value.asInstanceOf[AnyRef].getClass} to enum $e"))
          }
        case _ => Left(TypeDoesNotMatch(s"Unable to convert $value: ${value.asInstanceOf[AnyRef].getClass} to enum $e"))
      }
    }
  }

  implicit val columnToCallType: Column[CallType] = columnToEnum(CallType)
  implicit val columnToCallScope: Column[CallScope] = columnToEnum(CallScope)
  implicit val columnToCallTermination: Column[CallTermination] = columnToEnum(CallTermination)

  val simple = get[Long]("id") ~
    get[DateTime]("start_time") ~
    get[Option[DateTime]]("end_time") ~
    get[Boolean]("emitted") ~
    get[Option[String]]("caller_num") ~
    get[Option[String]]("callee_num") ~
    get[Option[DateTime]]("answer_time") ~
    get[Option[Long]]("ring_duration") ~
    get[CallType]("call_type") ~
    get[CallScope]("scope") ~
    get[Option[CallTermination]]("termination") ~
    get[Option[String]]("channel_interface") ~
    get[String]("unique_id") ~
    get[Option[Long]]("user_id") ~
    get[Option[Long]]("agent_id") ~
    get[Option[Long]]("queue_call_id") ~
    get[Option[Long]]("original_call_id") ~
    get[Option[Long]]("call_thread_id") ~
    get[Long]("call_thread_seq") map {
    case id ~ startTime ~ endTime ~ emitted ~ callerNum ~ calleeNum ~ answerTime ~ ringDuration ~ callType ~ scope ~
      termination ~ channelInterface ~ uniqueId ~ userId ~ agentId ~ queueCallId ~ originalCallId ~ callThreadId ~ callThreadSeq =>
      CallChannel(Some(id), startTime, endTime, emitted, callerNum, calleeNum, answerTime, ringDuration, callType, scope,
        termination, channelInterface, uniqueId, userId, agentId, queueCallId, originalCallId, deleted = false, None, callThreadId, callThreadSeq)
  }

  val callChannelInsert = SQL("""
                          INSERT INTO
                                xc_call_channel(
                                   start_time, end_time, emitted, caller_num, callee_num, answer_time, ring_duration, call_type, scope, termination, channel_interface, unique_id,
                                   user_id, agent_id, queue_call_id, original_call_id, call_thread_id, call_thread_seq)
                          VALUES (
                                   {start_time}, {end_time}, {emitted}, {caller_num}, {callee_num}, {answer_time}, {ring_duration},
                                   {call_type}::call_type, {scope}::call_scope_type, {termination}::call_termination_type, {channel_interface},
                                   {unique_id}, {user_id},{agent_id}, {queue_call_id}, {original_call_id}, {call_thread_id}, {call_thread_seq})""")

  val callChannelUpdate = SQL("""
                          UPDATE
                                xc_call_channel
                          SET
                                  start_time={start_time}, end_time={end_time}, emitted={emitted}, caller_num={caller_num},
                                  callee_num={callee_num}, answer_time={answer_time}, ring_duration={ring_duration}, call_type={call_type}::call_type, scope={scope}::call_scope_type,
                                  termination={termination}::call_termination_type, channel_interface={channel_interface}, unique_id={unique_id}, user_id={user_id},
                                  agent_id={agent_id}, queue_call_id={queue_call_id}, original_call_id={original_call_id},
                                  call_thread_id={call_thread_id}, call_thread_seq={call_thread_seq}
                          WHERE id={id}""")


  val logger = LoggerFactory.getLogger(getClass)

  def insertOrUpdate(callChannel: CallChannel)(implicit c: Connection): CallChannel = {
    if (callChannel.id.isDefined) update(callChannel)
    else insert(callChannel)
  }

  def insert(callChannel: CallChannel)(implicit c: Connection): CallChannel = {
    c.setAutoCommit(false)
    try {
      logger.info(s"Insert CallChannel - $callChannel")
      val id: Option[Long] = callChannelInsert.on(
        'start_time -> callChannel.startTime.toDate, 'end_time -> callChannel.endTime.map(_.toDate), 'emitted -> callChannel.emitted,
        'caller_num -> callChannel.callerNum, 'callee_num -> callChannel.calleeNum, 'answer_time -> callChannel.answerTime.map(_.toDate), 'ring_duration -> callChannel.ringDuration,
        'call_type -> callChannel.callType.toString, 'scope -> callChannel.scope.toString, 'termination -> callChannel.termination.map(_.toString),
        'channel_interface -> callChannel.channelInterface, 'unique_id -> callChannel.uniqueId, 'user_id -> callChannel.userId, 'agent_id -> callChannel.agentId,
        'queue_call_id -> callChannel.queueCallId, 'original_call_id -> callChannel.originalCallId,
        'call_thread_id -> callChannel.callThreadId, 'call_thread_seq -> callChannel.callThreadSeq).executeInsert()
      c.commit()
      c.setAutoCommit(true)
      val longId = id.map(_.toLong)
      callChannel.copy(id = longId, callThreadId = callChannel.callThreadId.orElse(longId))
    } catch {
      case e: Exception => c.rollback()
        logger.error("Got an exception while creating the call channel ", e)
        c.setAutoCommit(true)
        callChannel
    }
  }

  def update(callChannel: CallChannel)(implicit c: Connection): CallChannel = {
    c.setAutoCommit(false)
    try {
      logger.info(s"Update CallChannel - $callChannel")
      callChannelUpdate.on(
        'start_time -> callChannel.startTime.toDate, 'end_time -> callChannel.endTime.map(_.toDate), 'emitted -> callChannel.emitted,
        'caller_num -> callChannel.callerNum, 'callee_num -> callChannel.calleeNum, 'answer_time -> callChannel.answerTime.map(_.toDate), 'ring_duration -> callChannel.ringDuration,
        'call_type -> callChannel.callType.toString, 'scope -> callChannel.scope.toString, 'termination -> callChannel.termination.map(_.toString),
        'channel_interface -> callChannel.channelInterface, 'unique_id -> callChannel.uniqueId, 'user_id -> callChannel.userId, 'agent_id -> callChannel.agentId,
        'queue_call_id -> callChannel.queueCallId, 'original_call_id -> callChannel.originalCallId, 'id -> callChannel.id,
        'call_thread_id -> callChannel.callThreadId, 'call_thread_seq -> callChannel.callThreadSeq).executeInsert()
      c.commit()
      c.setAutoCommit(true)
    } catch {
      case e: Exception => c.rollback()
        logger.error("Got an exception while creating the call data ", e)
        c.setAutoCommit(true)
    }
    callChannel
  }

  def getByUniqueId(uniqueId: String)(implicit c: Connection): Option[CallChannel]  = {
    SQL("SELECT id, start_time, end_time, emitted, caller_num, callee_num, answer_time, ring_duration, call_type, " +
      "scope, termination, channel_interface, unique_id, user_id, agent_id, queue_call_id, original_call_id, coalesce(call_thread_id, id) as call_thread_id, call_thread_seq " +
      "FROM xc_call_channel WHERE unique_id = {uniqueId} LIMIT 1")
      .on('uniqueId -> uniqueId).as(simple.*).headOption
  }

  def getIdByUniqueId(uniqueId: String)(implicit c: Connection): Option[Long]  = {
    SQL("SELECT id FROM xc_call_channel WHERE unique_id = {uniqueId} LIMIT 1")
      .on('uniqueId -> uniqueId).as(get[Long]("id").singleOpt)
  }

  def getLastCallThreadSeq(callThreadId: Long)(implicit c: Connection): Long  = {
    SQL("SELECT COALESCE(max(call_thread_seq), 0) FROM xc_call_channel WHERE call_thread_id = {call_thread_id};")
      .on('call_thread_id -> callThreadId)
      .as(scalar[Long].single)
  }


  def getIdByuniqueIds(uniqueIds: List[String])(implicit c: Connection): List[Long] = {
    SQL("SELECT id FROM xc_call_channel WHERE unique_id IN ({uniqueIds}) AND answer_time IS NOT NULL")
      .on('uniqueIds -> uniqueIds).as(get[Long]("id").*)
  }

  def setStalePendingCalls()(implicit c: Connection) = {
    SQL("SELECT max(start_time) as max FROM xc_call_channel").as(get[Option[Date]]("max").single)
      .map { date =>
        SQL("UPDATE xc_call_channel SET end_time = start_time , termination = {termination}::call_termination_type WHERE end_time IS NULL AND start_time < {maxDate}::timestamp - INTERVAL '1 day'")
          .on('maxDate -> date, 'termination -> CallTermination.Stalled.toString).executeUpdate()
      }
  }

  def getPendingUniqueIds(oldestStartTime: DateTime)(implicit conn: Connection): List[String] = {
    SQL("SELECT unique_id FROM xc_call_channel WHERE start_time >= {oldestStartTime}")
      .on('oldestStartTime -> oldestStartTime.toDate)
      .as(get[String]("unique_id").*)
  }

  def getAllPendingUniqueIds(uniqueIds: List[String])(implicit conn: Connection): List[String] = {
    searchBackwardsByOriginalCallId(uniqueIds, 0) match {
      case Nil => uniqueIds
      case callThreadUniqueIds => callThreadUniqueIds
    }
  }

  private def searchBackwardsByOriginalCallId(recentPendingUniqueIds: List[String], counter: Int)(implicit conn: Connection): List[String] = {
    recentPendingUniqueIds match {
      case l if l.isEmpty => List()
      case _ if counter > 100 =>
        logger.error(s"Call thread search loop detected in following call channels id $recentPendingUniqueIds, stopping")
        List()
      case uniqueIdsList =>
        val originalCallIds: List[String] = SQL(s"SELECT original_call_id FROM xc_call_channel WHERE ${SqlUtils.createInClauseOrFalseFast("unique_id", uniqueIdsList)}")
          .as(get[Option[Int]]("original_call_id").*).flatten.map(_.toString)
        val pendingUniqueIds: List[String] = SQL(s"SELECT unique_id FROM xc_call_channel WHERE ${SqlUtils.createInClauseOrFalseFast("id", originalCallIds)}")
              .as(get[String]("unique_id").*)
        (recentPendingUniqueIds ++ searchBackwardsByOriginalCallId(pendingUniqueIds, counter + 1)).distinct
    }
  }

  def deleteById(callChannelUniqueIds: List[String])(implicit c: Connection): Unit = {
    if(callChannelUniqueIds.isEmpty)
      return
    c.setAutoCommit(false)
    try {
      val callChannelUniqueIdInClause = SqlUtils.createInClauseOrFalseFast("unique_id", callChannelUniqueIds)
      def deleteWhereIn(table: String, column: String) : String = s"DELETE FROM $table WHERE $column IN (SELECT id FROM xc_call_channel WHERE $callChannelUniqueIdInClause)"

      SQL(deleteWhereIn("xc_call_transfer","initial_call_id")).executeUpdate()
      SQL(deleteWhereIn("xc_call_conversation","caller_call_id")).executeUpdate()
      SQL(deleteWhereIn("xc_call_conversation","callee_call_id")).executeUpdate()
      SQL(s"DELETE FROM xc_call_channel WHERE $callChannelUniqueIdInClause").executeUpdate()

      c.commit()
    } catch {
      case e: Exception => c.rollback()
        throw e
    }
    c.setAutoCommit(true)
  }

  def getPendingStartTime()(implicit c: Connection): Option[DateTime] =
    SQL(
      s"""SELECT
                 MIN(start_time) as min_end_time
          FROM
                 xc_call_channel
          WHERE
                 end_time IS null AND start_time >= (SELECT max(start_time) as max FROM xc_call_channel) - INTERVAL '1 day'""".stripMargin)
      .as(get[Option[DateTime]]("min_end_time").single)
}

object CallType extends Enumeration {
  type CallType = Value
  val Acd = Value("acd")
  val Administrative = Value("administrative")
  val Consultation = Value("consultation")
  val Queued = Value("queued")
}

object CallScope extends Enumeration {
  type CallScope = Value
  val Internal = Value("internal")
  val External = Value("external")
}

object CallTermination extends Enumeration {
  type CallTermination = Value
  val Hangup = Value("hangup")
  val RemoteHangup = Value("remote_hangup")
  val SystemHangup = Value("system_hangup")
  val Stalled = Value("stalled")
}

case class CallChannel(id: Option[Long], startTime: DateTime, endTime: Option [DateTime], emitted: Boolean, callerNum: Option[String],
                       calleeNum: Option[String], answerTime: Option[DateTime], ringDuration: Option[Long], callType: CallType, scope: CallScope, termination: Option[CallTermination],
                       channelInterface: Option[String], uniqueId: String, userId: Option[Long], agentId: Option[Long],
                       queueCallId: Option[Long], originalCallId: Option[Long], deleted: Boolean, appName: Option[AppName], callThreadId: Option[Long], callThreadSeq: Long = 1) extends CallThreadEvent {

  def withAgent(agentId: Option[Long]) = {
    this.copy(agentId = agentId)
  }

  def withAnswer(time: Option[DateTime], getRingDuration: (CallChannel,Option[DateTime]) => Option[Long]) = {
    this.copy(
      answerTime =
        if (this.answerTime.isEmpty) time
        else this.answerTime,
      ringDuration = getRingDuration(this, time)
    )
  }

  def asNotDeleted = {
    this.copy(deleted = false)
  }

  def asDeleted = {
    this.copy(deleted = true)
  }
}
