package xivo.fullstats.model

import java.sql.Connection
import java.util.Date

import anorm.SqlParser._
import anorm.{SQL, ~}
import org.joda.time.DateTime
import xivo.fullstats.model.CallExitType.CallExitType

object CallExitType extends Enumeration {

  type CallExitType = Value
  val Full = Value("full")
  val Closed = Value("closed")
  val JoinEmpty = Value("joinempty")
  val LeaveEmpty = Value("leaveempty")
  val DivertCaRatio = Value("divert_ca_ratio")
  val DivertWaitTime = Value("divert_waittime")
  val Answered = Value("answered")
  val Abandoned = Value("abandoned")
  val Timeout = Value("timeout")
  val ExitWithKey = Value("exit_with_key")
}

case class CallOnQueue(var id: Option[Int], var callid: Option[String], var queueTime: DateTime, var ringSeconds: Int, var answerTime: Option[DateTime],
                       var hangupTime: Option[DateTime], var status: Option[CallExitType], var queueRef: String, var agentNum: Option[String]) extends CallSummary {
  var linkedIdFound = true
  def isLinkedIdFound = linkedIdFound
  def setLinkedIdFound(linkedIdFound: Boolean) = this.linkedIdFound = linkedIdFound

  override def getStartTime: DateTime = queueTime
}

object CallOnQueue extends CallSummaryTable[CallOnQueue] {

  def deleteStalePendingCalls()(implicit conn: Connection): Unit =
    SQL("SELECT max(queue_time) AS val FROM call_on_queue").as(get[Option[Date]]("val") *).head match {
      case None =>
      case Some(max) => deleteById(SQL(
        """SELECT id FROM call_on_queue WHERE hangup_time IS NULL
          AND queue_time < {maxDate}::timestamp - INTERVAL '1 day'""").on('maxDate -> max).as(get[Int]("id") *))
    }

  val simple = get[Option[String]]("callid") ~
    get[Date]("queue_time") ~
    get[Int]("total_ring_seconds") ~
    get[Option[Date]]("answer_time") ~
    get[Option[Date]]("hangup_time") ~
    get[Option[String]]("status") ~
    get[String]("queue_ref") ~
    get[Option[String]]("agent_num") map {
    case callid ~ queuetime ~ ringseconds ~ answertime ~ hanguptime ~ status ~ queue ~ agent =>
      CallOnQueue(None, callid, new DateTime(queuetime), ringseconds, answertime.map(new DateTime(_)), hanguptime.map(new DateTime(_)),
        status.map(CallExitType.withName), queue, agent)
  }

  def deleteByCallid(callids: List[String])(implicit conn: Connection) = {
    val inClause = SqlUtils.createInClauseOrFalse("callid", callids)
    SQL(s"DELETE FROM call_on_queue WHERE $inClause").executeUpdate()
  }

  def deleteById(ids: List[Int])(implicit conn: Connection) = {
    val inClause = SqlUtils.createInClauseOrFalse("id", ids)
    SQL(s"DELETE FROM call_on_queue WHERE $inClause").executeUpdate()
  }

  def getPendingCallIds()(implicit conn: Connection): List[String] =
    SQL(s"SELECT callid FROM call_on_queue WHERE hangup_time IS NULL").as(get[String]("callid") *)

  def getFormerlyPendingCalls(pendingCalls: List[CallOnQueue])(implicit conn: Connection): List[CallOnQueue] = pendingCalls match {
    case List() => List()
    case head::tail => SQL(s"""SELECT callid, queue_time, total_ring_seconds, answer_time, hangup_time, status::varchar AS status, queue_ref, agent_num FROM call_on_queue
            WHERE queue_time = {queue_time} AND callid = {callid} AND status IS NOT NULL""")
        .on('queue_time -> head.queueTime.toDate, 'callid -> head.callid)
        .as(simple *):::getFormerlyPendingCalls(tail)
  }

  def getAllWithNullStatus()(implicit conn: Connection): List[CallOnQueue] = SQL(s"""SELECT callid, queue_time, total_ring_seconds, answer_time, hangup_time, status::varchar AS status, queue_ref, agent_num FROM call_on_queue
         WHERE status IS NULL""").as(simple *)

  override def insertOrUpdate(call: CallOnQueue)(implicit c: Connection): CallOnQueue = {
    if(call.id.isDefined) update(call)
    else insert(call)
  }

  private def insert(call: CallOnQueue)(implicit c: Connection): CallOnQueue = {
    val id: Option[Long] = SQL("""INSERT INTO call_on_queue(callid, queue_time, total_ring_seconds, answer_time, hangup_time, status, queue_ref, agent_num)
      VALUES ({callid}, {queue_time}, {total_ring_seconds}, {answer_time}, {hangup_time}, {status}::call_exit_type, {queue_ref}, {agent_num})""").on(
        'callid -> call.callid, 'queue_time -> call.queueTime.toDate, 'total_ring_seconds -> call.ringSeconds, 'answer_time -> call.answerTime.map(_.toDate),
        'hangup_time -> call.hangupTime.map(_.toDate), 'status -> call.status.map(_.toString), 'queue_ref -> call.queueRef, 'agent_num -> call.agentNum)
      .executeInsert()
    call.copy(id=id.map(_.toInt))
  }

  private def update(call: CallOnQueue)(implicit c: Connection): CallOnQueue = {
    SQL("UPDATE call_on_queue SET callid={callid}, queue_time={queue_time}, total_ring_seconds={total_ring_seconds}, answer_time={answer_time}, hangup_time={hangup_time}," +
      "status={status}::call_exit_type, queue_ref={queue_ref}, agent_num={agent_num} WHERE id={id}").on('callid -> call.callid, 'queue_time -> call.queueTime.toDate,
        'total_ring_seconds -> call.ringSeconds, 'answer_time -> call.answerTime.map(_.toDate), 'hangup_time -> call.hangupTime.map(_.toDate),
        'status -> call.status.map(_.toString), 'queue_ref -> call.queueRef, 'agent_num -> call.agentNum, 'id -> call.id).executeUpdate()
    call
  }
}
