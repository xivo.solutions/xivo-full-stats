package xivo.fullstats.model

import java.sql.Connection

import anorm.SqlParser._
import anorm._

trait LastIdTable {
  
  val lastIdTable: String

  def setLastId(id: Int)(implicit connection: Connection): Unit = getIdOption match {
    case None => SQL(s"INSERT INTO $lastIdTable(id) VALUES ({id})").on('id -> id).executeUpdate
    case Some(_) => SQL(s"UPDATE $lastIdTable SET id = {id}").on('id -> id).executeUpdate
  }

  def getLastId()(implicit connection: Connection): Int = getIdOption.getOrElse(0)

  private def getIdOption(implicit c: Connection) = SQL(s"SELECT id FROM $lastIdTable").as(get[Int]("id") *).headOption
}