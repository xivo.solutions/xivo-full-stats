package xivo.fullstats.callparsers.cel

import org.joda.time.DateTime
import xivo.fullstats.model.{Cel, CallData}

class HangedUp(callData: CallData, cel: Cel) extends CelState(callData, true) {
  result.endTime = result.transferTime orElse Some(new DateTime(cel.eventTime))
  result.holdPeriods.headOption.foreach(p => p.end match {
    case None => p.end = Some(new DateTime(cel.eventTime))
    case Some(_) =>
  })
  override def processCel(cel: Cel): CelState = {
    throw new InvalidCelException
  }
}