package xivo.fullstats.callparsers.cel

import xivo.fullstats.callparsers.State
import xivo.fullstats.model.{AttachedData, CallData, Cel}

abstract class CelState(callData: CallData, isFinished: Boolean) extends State[Cel, CallData](isFinished) {
  override var result = callData

  override def processEvent(cel: Cel): CelState = {
    if (cel.eventType equals "ATTACHED_DATA") {
      extractedAttachedData(cel.appData).foreach(attachedData => result.addAttachedData(attachedData))
      this
    } else {
      processCel(cel)
    }
  }

  private def extractedAttachedData(userField: String): Option[AttachedData] = {
    val splitted = userField.split(",", 2)
    if (splitted.size > 1) {
      val keyValue = splitted(1).split("=")
      if (keyValue.size > 1)
        return Some(AttachedData(keyValue(0), keyValue(1)))
    }
    None
  }

  def processCel(cel: Cel): CelState
}

class InvalidCelException extends Exception
