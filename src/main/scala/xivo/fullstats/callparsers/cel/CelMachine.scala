package xivo.fullstats.callparsers.cel

import java.sql.Connection

import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model.{CallData, Cel}
import xivo.fullstats.callparsers.{State, StateMachine}

class CelMachine(implicit c: Connection) extends StateMachine[Cel, CallData](CallData) {
  var channelMgr: Option[ChannelManager] = None

  override def getInitialState: State[Cel, CallData] = new Off(EmptyObjectProvider.emptyCallData())

  override def forceCloture() = {
    val res = state.getResult
    if(res.endTime.isEmpty) {
      res.endTime = Some(res.answerTime.getOrElse(res.startTime))
      res.holdPeriods = res.holdPeriods.map(hp => if(hp.end.isDefined) hp else hp.copy(end = Some(hp.start)))
      channelMgr.foreach(_.forceCloture())
    }
  }

  override def processEvent(cel: Cel): Unit = {
    super.processEvent(cel)
    if(channelMgr.isEmpty) {
      state.getResult.id match {
        case None =>
        case Some(id) => channelMgr = Some(new ChannelManager(id))
      }
    }
    channelMgr.foreach(_.processEvent(cel))
  }
}