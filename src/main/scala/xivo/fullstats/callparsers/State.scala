package xivo.fullstats.callparsers

abstract class State[E, R](isFinal: Boolean) {
  var result: R

  def processEvent(event: E): State[E, R]
  def getResult: R = result
  def setResult(r: R) = result = r
  def isCallFinished: Boolean = isFinal
}
