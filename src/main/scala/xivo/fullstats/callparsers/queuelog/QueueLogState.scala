package xivo.fullstats.callparsers.queuelog

import org.slf4j.LoggerFactory
import xivo.fullstats.callparsers.State
import xivo.fullstats.model.{CallOnQueue, QueueLog}

abstract class QueueLogState(call: CallOnQueue, isFinished: Boolean) extends State[QueueLog, CallOnQueue](isFinished) {
  val logger = LoggerFactory.getLogger(getClass)
  var result = call

  protected def processUnexpectedQueueLog(ql: QueueLog) =  {
    logger.info(s"Unexpected queue log $ql on state $this")
    throw new UnexpectedQueueLogException(ql)
  }
}

class UnexpectedQueueLogException(ql: QueueLog) extends Exception

class NoSuchCelException(uniqueId: String) extends Exception {
  override def getMessage: String = s"Could not find a cel with the uniqueid $uniqueId"
}