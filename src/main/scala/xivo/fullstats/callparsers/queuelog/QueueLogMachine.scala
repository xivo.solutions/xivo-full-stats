package xivo.fullstats.callparsers.queuelog

import java.sql.Connection

import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model.{CallOnQueue, QueueLog}
import xivo.fullstats.callparsers.{State, StateMachine}

class QueueLogMachine(implicit val c: Connection) extends StateMachine[QueueLog, CallOnQueue](CallOnQueue) {
  override def getInitialState: State[QueueLog, CallOnQueue] = new InitialState(EmptyObjectProvider.emptyCallOnQueue())
  override def forceCloture() = {
    val res = state.getResult
    if(res.hangupTime.isEmpty)
      res.hangupTime = Some(res.answerTime.getOrElse(res.queueTime))
  }
}