package xivo.fullstats.streams.conversation.cel

import java.sql.Connection

import akka.NotUsed
import akka.stream.scaladsl.Flow
import xivo.fullstats.model.{ChanEndCel, Conversation, ConversationLinks}
import xivo.fullstats.streams.conversation.cel.ChanEnd.CallerLinkedWithCalled
import xivo.fullstats.streams.conversation.{ConversationFlow, ConversationRepository}

object ChanEnd {
  case class CallerLinkedWithCalled(callerCallChannelId: Long, calledCallChannelIds: List[Long])
}

class ChanEnd(implicit connection: Connection, conversationRepository: ConversationRepository) extends ConversationFlow[ChanEndCel] {

  val flow: Flow[ChanEndCel, Conversation, NotUsed] = Flow[ChanEndCel]
    .via(filterByContext(List("agentcallback", "xuc_attended_xfer_wait", "xuc_attended_xfer_end")))
    .via(filterByApp(List("AppDial2")))
    .map(log)
    .via(getConversationLinks)
    .collect(nonEmptyResultOfConversationLinks)
    .alsoTo(deleteFromMap)
    .collect(nonEmptyListOfRelatedIds)
    .map(processConversation)
    .collect(nonEmptyResultOfLinkedCallerAndCalled)
    .map(createConversation)
    .mapConcat(identity)

  private def processConversation(conversationLinks: ConversationLinks): Option[CallerLinkedWithCalled] = {
    conversationLinks.getCallerCallChannelId
        .map(callerId => CallerLinkedWithCalled(callerId, conversationLinks.getCalledCallChannelIds))
  }

  private def createConversation(linkedCallerWithCalled: CallerLinkedWithCalled): List[Conversation] = {
    linkedCallerWithCalled.calledCallChannelIds
      .collect { case calleeId if linkedCallerWithCalled.callerCallChannelId != calleeId =>
        Conversation(None, linkedCallerWithCalled.callerCallChannelId, calleeId)
      }
  }

  def nonEmptyResultOfConversationLinks: PartialFunction[Option[ConversationLinks], ConversationLinks] = { case Some(c) => c }
  def nonEmptyListOfRelatedIds: PartialFunction[ConversationLinks, ConversationLinks] = { case c if c.relatedUniqueIds.nonEmpty => c }
  def nonEmptyResultOfLinkedCallerAndCalled: PartialFunction[Option[CallerLinkedWithCalled], CallerLinkedWithCalled] = { case Some(c) => c }
}
