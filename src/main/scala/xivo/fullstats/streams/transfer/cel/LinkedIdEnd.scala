package xivo.fullstats.streams.transfer.cel

import java.sql.Connection

import akka.NotUsed
import akka.stream.scaladsl.Flow
import xivo.fullstats.model._
import xivo.fullstats.streams.transfer.{TransferFlow, TransferRepository}

class LinkedIdEnd(implicit connection: Connection, transferRepository: TransferRepository) extends TransferFlow[LinkedIdEndCel] {

  val flow: Flow[LinkedIdEndCel, List[TransferLinks], NotUsed] = Flow[LinkedIdEndCel]
    .map(keepCel)
    .map(log)
    .via(getTransferLinks)
    .alsoTo(deleteFromMap)
    .collect{ case Some(t) => t }
}
