package xivo.fullstats.streams.transfer.cel

import java.sql.Connection

import akka.NotUsed
import akka.stream.scaladsl.Flow
import xivo.fullstats.model.{TransferLinks, XivoAttendedTransferEndCel}
import xivo.fullstats.streams.transfer.{TransferFlow, TransferRepository}

class XivoAttendedTransferEnd(implicit connection: Connection, transferRepository: TransferRepository) extends TransferFlow[XivoAttendedTransferEndCel] {

  val flow: Flow[XivoAttendedTransferEndCel, List[TransferLinks], NotUsed] = Flow[XivoAttendedTransferEndCel]
    .map(keepCel)
    .map(log)
    .via(getTransferLinks)
    .map(processAttendedTransfer)

  private def processAttendedTransfer(maybeTransferLinks: Option[List[TransferLinks]]): List[TransferLinks] = {
    maybeTransferLinks match {
      case Some(transferLinks) => createAndUpdate(transferLinks)
      case None => List()
    }
  }

  private def updateTransferLink(t: TransferLinks): TransferLinks = {
    t.copy(transferDecisionId = Some(savedCel.linkedId), transferTime = Some(savedCel.eventTime))
  }

  private def createAndUpdate(transferLinks: List[TransferLinks]) = {
    transferLinks.map {
      case t if t.transferDecisionId.isEmpty || t.transferDecisionId.contains(savedCel.linkedId) => updateTransferLink(t)
      case t => t
    }
  }
}
