package xivo.fullstats.streams.transfer.cel

import java.sql.Connection

import akka.NotUsed
import akka.stream.scaladsl.Flow
import xivo.fullstats.model.{BlindTransferCel, TransferLinks}
import xivo.fullstats.streams.transfer.{TransferFlow, TransferRepository}

class BlindTransfer(implicit connection: Connection, transferRepository: TransferRepository) extends TransferFlow[BlindTransferCel] {

  val flow: Flow[BlindTransferCel, List[TransferLinks], NotUsed] = Flow[BlindTransferCel]
    .map(keepCel)
    .map(log)
    .via(getTransferLinks)
    .map(processAttendedTransfer)

  private def processAttendedTransfer(maybeTransferLinks: Option[List[TransferLinks]]): List[TransferLinks] = {
    maybeTransferLinks match {
      case Some(transferLinks) => createAndUpdate(transferLinks)
      case None => List()
    }
  }

  private def updateTransferLink(t: TransferLinks): TransferLinks = {
    t.copy(transferDecisionId = Some(savedCel.linkedId), onHold = Some(savedCel.uniqueId), transferTime = Some(savedCel.eventTime))
  }

  private def createAndUpdate(transferLinks: List[TransferLinks]) = {
    transferLinks.flatMap {
      case t if t.transferDecisionId.isEmpty => List(updateTransferLink(t))
      case t if t.transferDecisionId.contains(savedCel.linkedId) =>
        val newTransfer = { if (t.stopCollecting) None else Some(updateTransferLink(t)) }
        val oldTransfer = t.copy(stopCollecting = true)
        List(Some(oldTransfer), newTransfer).flatten
      case t => List(t)
    }
  }
}
