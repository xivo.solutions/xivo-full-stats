package xivo.fullstats.streams.transfer.cel

import java.sql.Connection

import akka.NotUsed
import akka.stream.scaladsl.Flow
import xivo.fullstats.model.{BridgeEnterCel, TransferLinks}
import xivo.fullstats.streams.transfer.{TransferFlow, TransferRepository}

class BridgeEnter(implicit connection: Connection, transferRepository: TransferRepository) extends TransferFlow[BridgeEnterCel] {

  val flow: Flow[BridgeEnterCel, List[TransferLinks], NotUsed] = Flow[BridgeEnterCel]
    .via(filterByContext(List("agentcallback", "xuc_attended_xfer_wait", "xuc_attended_xfer_end", "transfer")))
    .map(keepCel)
    .map(log)
    .via(getTransferLinks)
    .map(processTransferLinks)

  private def processTransferLinks(maybeTransferLinks: Option[List[TransferLinks]]): List[TransferLinks] = {
    maybeTransferLinks match {
      case Some(transferLinks) => updateExistingTransferLink(transferLinks)
      case None => createNewTransferLink
    }
  }
}
