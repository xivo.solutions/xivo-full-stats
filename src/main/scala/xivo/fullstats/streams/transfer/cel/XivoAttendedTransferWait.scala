package xivo.fullstats.streams.transfer.cel

import java.sql.Connection

import akka.NotUsed
import akka.stream.scaladsl.Flow
import xivo.fullstats.model.{TransferLinks, XivoAttendedTransferWaitCel}
import xivo.fullstats.streams.transfer.{TransferFlow, TransferRepository}

class XivoAttendedTransferWait(implicit connection: Connection, transferRepository: TransferRepository) extends TransferFlow[XivoAttendedTransferWaitCel] {

  val flow: Flow[XivoAttendedTransferWaitCel, List[TransferLinks], NotUsed] = Flow[XivoAttendedTransferWaitCel]
    .map(keepCel)
    .map(log)
    .via(getTransferLinks)
    .map(processAttendedTransfer)

  private def processAttendedTransfer(maybeTransferLinks: Option[List[TransferLinks]]): List[TransferLinks] = {
    maybeTransferLinks match {
      case Some(transferLinks) if savedCel.uniqueId != savedCel.linkedId => createAndUpdate(transferLinks)
      case Some(transferLinks) => transferLinks
      case None => List()
    }
  }

  private def updateTransferLink(t: TransferLinks): TransferLinks = {
    t.copy(transferDecisionId = None, onHold = Some(savedCel.uniqueId), transferTime = None)
  }

  private def createAndUpdate(transferLinks: List[TransferLinks]): List[TransferLinks] = {
    transferLinks.flatMap {
      case t if t.transferDecisionId.isEmpty => List(updateTransferLink(t))
      case t if t.transferDecisionId.contains(savedCel.linkedId) =>
        val newTransfer = { if (t.stopCollecting) None else Some(updateTransferLink(t)) }
        val oldTransfer = updateExistingTransferLink(List(t)).head.copy(stopCollecting = true)
        List(Some(oldTransfer), newTransfer).flatten
      case t => List(t)
    }
  }

}
