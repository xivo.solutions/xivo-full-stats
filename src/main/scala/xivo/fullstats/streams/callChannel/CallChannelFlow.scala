package xivo.fullstats.streams.callChannel

import akka.NotUsed
import akka.stream.scaladsl.Flow
import org.joda.time.{DateTime, Seconds}
import org.slf4j.LoggerFactory
import xivo.fullstats.model.CallType.CallType
import xivo.fullstats.model.CelAppName.AppName
import xivo.fullstats.model.{CallChannel, CallType, Cel, TypedCelEvent}
import xivo.fullstats.streams.callChannel.CallChannelFlow.{hasUserId, isAnswered, isCurrentCaller, isOngoing, matchCallType}

object CallChannelFlow {
  type CallChannelFilter = CallChannel => Option[CallChannel]

  implicit def maybeCallChannelToBoolean(callChannel: Option[CallChannel]): Boolean = callChannel.isDefined

  def matchCallType(callType: CallType): CallChannelFilter = { callChannel: CallChannel => if (callChannel.callType == callType) Some(callChannel) else None }
  def isAnswered: CallChannelFilter = { callChannel: CallChannel => if (callChannel.answerTime.isDefined) Some(callChannel) else None }
  def isOngoing: CallChannelFilter = { callChannel: CallChannel => if (callChannel.endTime.isEmpty) Some(callChannel) else None }
  def isCurrentCaller(callee: CallChannel): CallChannelFilter = { caller: CallChannel => if (caller.userId == callee.userId) Some(caller) else None }
  def isCurrentAgent(agentId: Long): CallChannelFilter = { caller: CallChannel => if (caller.agentId.contains(agentId)) Some(caller) else None }
  def hasUserId: CallChannelFilter = { callChannel: CallChannel => if (callChannel.userId.isDefined) Some(callChannel) else None }
  def isAppName(appName: AppName): CallChannelFilter = { callChannel: CallChannel => if (callChannel.appName.contains(appName)) Some(callChannel) else None }
}

trait CallChannelFlow[T <: TypedCelEvent] {
  val logger = LoggerFactory.getLogger(getClass)

  val flow: Flow[T, CallChannel, NotUsed]
  var savedCel: Cel = _

  def log(c: T) = {
    logger.info(s"Processing event $c")
    c
  }

  def keepCel(c: T) = {
    savedCel = c.cel
    c
  }

  def getByAppName(appNameList: List[String]) = Flow[T].collect{
    case c if appNameList.contains(c.cel.appName) => c
  }

  def filterByContext(contextList: List[String]) = Flow[T].collect{
    case c if !contextList.contains(c.cel.context) => c
  }

  def filterByChannelName(channelNameList: List[String]) = Flow[T].collect{
    case c if !channelNameList.exists(c.cel.chanName.contains(_)) => c
  }

  def getCallChannel(implicit repository: CallChannelRepository) = Flow[T]
    .map(c => repository.getCallFromMap(c.cel.uniqueId))
    .collect { case Some(callChannel) => callChannel }

  def getByUniqueIdOrLinkedId(implicit repository: CallChannelRepository) = Flow[T]
    .map(c => repository.getCallFromMap(c.cel.uniqueId).orElse(repository.getCallFromMap(c.cel.linkedId)))
    .collect { case Some(callChannel) => callChannel }

  def deleteCallChannelFromRepository(implicit repository: CallChannelRepository) = Flow[T]
    .map(c => repository.deleteCallFromMap(c.cel.uniqueId))
    .collect { case Some(callChannel) => callChannel.asDeleted }

  def deleteLinkedCallChannelFromRepository(implicit repository: CallChannelRepository) = Flow[T]
    .map { c =>
      List(repository.deleteCallFromMap(c.cel.uniqueId),
        repository.deleteCallFromMap(c.cel.linkedId))
    }
    .mapConcat(identity)
    .collect { case Some(callChannel) => callChannel.asDeleted }

  def finishAndSave(callChannel: CallChannel)(implicit repository: CallChannelRepository) = {
    repository.addCallToMap(callChannel.uniqueId, callChannel).asNotDeleted
  }

  def findOngoingWithSameUserId(callChannel: CallChannel)(implicit repository: CallChannelRepository): Option[CallChannel] = {
    def ongoingWithSameUserIdCondition(callee: CallChannel): CallChannel => Boolean = {
      caller: CallChannel =>
      isAnswered(caller)
        .filter(_.uniqueId != callee.uniqueId)
        .flatMap(hasUserId)
        .flatMap(isOngoing)
        .flatMap(isCurrentCaller(callee)).isDefined
    }
    repository.repository.find(c => ongoingWithSameUserIdCondition(callChannel)(c._2)).map(_._2)
  }

  def getRingDuration(callChannel: CallChannel, answerTime: Option[DateTime])(implicit repository: CallChannelRepository): Option[Long] = {
    if (callChannel.ringDuration.isEmpty)
      Some(Seconds.secondsBetween(callChannel.startTime, answerTime.getOrElse(callChannel.startTime)).getSeconds)
    else callChannel.ringDuration
  }
}
