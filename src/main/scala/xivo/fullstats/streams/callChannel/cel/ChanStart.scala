package xivo.fullstats.streams.callChannel.cel

import java.sql.Connection

import akka.NotUsed
import akka.stream.scaladsl.Flow
import xivo.fullstats.model._
import xivo.fullstats.streams.callChannel.CallChannelFlow._
import xivo.fullstats.streams.callChannel.{CallChannelFlow, CallChannelRepository}

import scala.util.Try

class ChanStart(implicit repository: CallChannelRepository, connection: Connection) extends CallChannelFlow[ChanStartCel] {

  val flow: Flow[ChanStartCel, CallChannel, NotUsed] = Flow[ChanStartCel]
    .via(filterByContext(List("agentcallback", "transfer")))
    .via(filterByChannelName(List("Local/")))
    .map(keepCel)
    .map(log)
    .map(_ => createCallChannel)
    .map(setCallChannelType)
    .map(setOriginalCallId)
    .map(setQueueCallId)
    .map(finishAndSave)

  private def createCallChannel: CallChannel = CallChannel(
    id = None,
    startTime = savedCel.eventTime,
    endTime = None,
    emitted = false,
    callerNum = if (savedCel.cidNum.trim.isEmpty) None else Some(savedCel.cidNum),
    calleeNum = if (savedCel.exten.trim.isEmpty || savedCel.exten.equals("s")) None else Some(savedCel.exten),
    answerTime = None,
    ringDuration = None,
    callType = CallType.Administrative,
    scope =  if (savedCel.cidNum.trim.isEmpty) CallScope.External else CallScope.Internal,
    termination = None,
    channelInterface = getChannelInterface(savedCel.chanName),
    uniqueId = savedCel.uniqueId,
    userId = getUserId(savedCel.cidNum),
    agentId = None,
    queueCallId = None,
    originalCallId = None,
    deleted = false,
    appName = None,
    callThreadId = None,
    callThreadSeq = 1
  )

  private def setCallChannelType(callChannel: CallChannel): CallChannel = {
    def setAcdToCallee(callChannel: CallChannel): CallChannel = {
      if (hasUserId(callChannel)) {
        findOriginatorBlindTransferCallChannel match {
          case Some(c) if matchCallType(CallType.Queued)(c) || matchCallType(CallType.Consultation)(c) =>
            callChannel.copy(callType = CallType.Acd)
          case _ => repository.getCallFromMap(savedCel.linkedId) match {
              case Some(c) if (matchCallType(CallType.Queued)(c) || matchCallType(CallType.Consultation)(c)) && isAppName(CelAppName.QueueApp)(c) =>
                callChannel.copy(callType = CallType.Acd)
              case _ => callChannel
            }
        }
      }
      else callChannel
    }
    setAcdToCallee(callChannel)
  }

  private def setOriginalCallId(callChannel: CallChannel): CallChannel = {
    findOngoingWithSameUserId(callChannel)
      .orElse(findOriginatorBlindTransferCallChannel)
      .orElse(repository.getCallFromMap(savedCel.linkedId)) match {
        case Some(c) =>
          val nextCallThreadSeq: Long = c.callThreadId
            .map(CallChannel.getLastCallThreadSeq(_))
            .map(Math.max(_, c.callThreadSeq) + 1)
            .getOrElse(1)
          callChannel.copy(originalCallId = c.id, callThreadId = c.callThreadId, callThreadSeq = nextCallThreadSeq)
        case _ =>
          callChannel.copy(originalCallId = getCallChannelId)
    }
  }

  private def setQueueCallId(callChannel: CallChannel): CallChannel = {
    def getQueueCallId = Cel.getUniqueIdFromLinkedId(savedCel.linkedId, "APP_START", "Queue", "queue")
      .flatMap(id => QueueCall.getQueueCallIdByUniqueId(id, savedCel.eventTime))

    matchCallType(CallType.Acd)(callChannel)
      .map(c => c.copy(queueCallId = getQueueCallId))
      .getOrElse(callChannel)
  }
  private def getChannelInterface(channel: String): Option[String] = Try(channel.substring(0, channel.lastIndexOf("-"))).toOption

  private def getUserId(number: String): Option[Long] = Extension.getExtensionByNumber(number).map(_.typeval.toLong)

  private def findOriginatorBlindTransferCallChannel: Option[CallChannel] = {
    Cel.getUniqueIdFromLinkedId(linkedid = savedCel.linkedId, eventType = "BLINDTRANSFER")
      .flatMap(CallChannel.getByUniqueId)
  }

  private def getCallChannelId: Option[Long] = repository.getCallFromMap(savedCel.linkedId).flatMap(_.id)
}
