package xivo.fullstats.streams.callChannel.cel

import akka.NotUsed
import akka.stream.scaladsl.Flow
import xivo.fullstats.model
import xivo.fullstats.model.CelAppName.{DialApp, QueueApp}
import xivo.fullstats.model._
import xivo.fullstats.streams.callChannel.{CallChannelFlow, CallChannelRepository}

class AppStart(implicit repository: CallChannelRepository) extends CallChannelFlow[AppStartCel] {

  val flow: Flow[AppStartCel, CallChannel, NotUsed] = Flow[AppStartCel]
    .map(keepCel)
    .via(getByAppName(List("Dial","Queue")))
    .via(filterByContext(List("agentcallback")))
    .map(log)
    .via(getByUniqueIdOrLinkedId)
    .map(setCallType)
    .map(setAppNameFlag)
    .map(setCallDirection)
    .map(setCalleeNum)
    .map(finishAndSave)

  private def setCallType(callChannel: CallChannel): CallChannel = {
    callChannel.copy(callType = setCallTypeByAppName(callChannel))
  }

  private def setAppNameFlag(callChannel: CallChannel): CallChannel = {
    savedCel.appName match {
      case "Queue" => callChannel.copy(appName = Some(QueueApp))
      case "Dial" => callChannel.copy(appName = Some(DialApp))
      case _ => callChannel
    }
  }

  private def setCallDirection(callChannel: CallChannel): CallChannel = {
    savedCel.appName match {
      case "Dial" | "Queue" => callChannel.copy(emitted = true)
      case _ => callChannel
    }
  }

  private def setCalleeNum(callChannel: CallChannel): CallChannel = {
    savedCel.appName match {
      case "Dial" if callChannel.emitted && callChannel.calleeNum.isEmpty && savedCel.cidNum.nonEmpty =>
        callChannel.copy(calleeNum = Some(savedCel.cidNum))
      case _ => callChannel
    }
  }

  private def setCallTypeByAppName(callChannel: CallChannel): model.CallType.Value = {
    val callType = callChannel.callType

    savedCel.appName match {
      case "Queue" | "Dial" if findOngoingWithSameUserId(callChannel).isDefined => CallType.Consultation
      case "Queue" if callType != CallType.Consultation => CallType.Queued
      case _ if callType == CallType.Consultation => CallType.Consultation
      case _ if callType == CallType.Queued => CallType.Queued
      case _ => CallType.Administrative
    }
  }
}
