package xivo.fullstats.streams.callChannel.cel

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.fasterxml.jackson.core.JsonParseException
import play.api.libs.json.{JsValue, Json}
import xivo.fullstats.model._
import xivo.fullstats.streams.callChannel.{CallChannelFlow, CallChannelRepository}

class Hangup(implicit repository: CallChannelRepository) extends CallChannelFlow[HangupCel] {

  val flow: Flow[HangupCel, CallChannel, NotUsed] = Flow[HangupCel]
    .map(keepCel)
    .via(filterByContext(List("agentcallback", "transfer")))
    .map(log)
    .via(getCallChannel)
    .map(processHangup)
    .map(setCallerNum)
    .map(finishAndSave)

  private def processHangup(callChannel: CallChannel) = {
    callChannel.copy(
      endTime = Some(savedCel.eventTime),
      ringDuration = getRingDuration(callChannel, Some(savedCel.eventTime)),
      termination = Some(isHangupByMe(savedCel.chanName, parseExtraData(savedCel.extra)))
    )
  }

  private def setCallerNum(callChannel: CallChannel) = {
    callChannel.callerNum match {
      case None if savedCel.cidNum.nonEmpty => callChannel.copy(callerNum = Some(savedCel.cidNum))
      case _ => callChannel
    }
  }

  private def isHangupByMe(channelName: String, hangupSource: Option[String]) = {
    val maybeInterface = channelName.split("-").headOption
    val maybeSourceInterface: Option[String] = hangupSource.flatMap(_.split("-").headOption)

    (maybeInterface, maybeSourceInterface) match {
      case (Some(interface), Some(sourceInterface)) if interface.toLowerCase == sourceInterface.toLowerCase => CallTermination.Hangup
      case _ => CallTermination.RemoteHangup
    }
  }

  private def parseExtraData(celExtraData: String): Option[String] = {
    if (celExtraData.nonEmpty) {
      try {
        val json: JsValue = Json.parse(celExtraData)
        (json \ "hangupsource").asOpt[String]
      } catch {
        case e: JsonParseException => None
      }
    }
    else
      None
  }
}
