package xivo.fullstats.streams.queueCall

import xivo.fullstats.model.QueueCall

import scala.collection.mutable

class QueueCallRepository {

  var repository: mutable.HashMap[String, QueueCall] = mutable.HashMap()

  def addCallToMap(key: String, value: QueueCall): QueueCall = {
    repository += (key -> value)
    value
  }

  def getCallFromMap(key: String): Option[QueueCall] = {
    repository.get(key)
  }

  def getByTransferred(key: String): Option[QueueCall] = {
    repository
      .find(_._2.transferredCalls.contains(key))
      .map(_._2)
  }

  def deleteCallFromMap(key: String): Option[QueueCall] = {
    repository.remove(key)
  }
}
