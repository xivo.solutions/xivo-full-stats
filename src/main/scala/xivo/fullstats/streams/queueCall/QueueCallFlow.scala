package xivo.fullstats.streams.queueCall

import java.sql.Connection

import akka.NotUsed
import akka.stream.scaladsl.Flow
import org.slf4j.LoggerFactory
import xivo.fullstats.model.QueueTerminationType.QueueTerminationType
import xivo.fullstats.model._

import scala.util.Try

trait QueueCallFlow[T <: TypedQueueLog] {
  val logger = LoggerFactory.getLogger(getClass)

  val flow: Flow[T, QueueCall, NotUsed]
  var savedQl: QueueLog = _

  def log(c: T) = {
    logger.info(s"Processing event $c")
    c
  }

  def keepQl(c: T) = {
    savedQl = c.ql
    c
  }

  def finishAndSave(
      queueCall: QueueCall
  )(implicit repository: QueueCallRepository) = {
    repository.addCallToMap(queueCall.uniqueId, queueCall)
  }

  def updateTerminationType(queueCall: QueueCall): QueueCall = {
    val terminationType: Option[QueueTerminationType] = Try(
      QueueTerminationType.withName(savedQl.event)
    ).toOption.map(_.asInstanceOf[QueueTerminationType])
    queueCall.copy(termination = terminationType)
  }

  private def getFromMap(implicit
      repository: QueueCallRepository
  ): Option[QueueCall] =
    repository.getCallFromMap(savedQl.callid)
  private def getFromData2(implicit
      repository: QueueCallRepository,
      connection: Connection
  ): Option[QueueCall] =
    findLinkedIdFromData2.flatMap(repository.getCallFromMap)
  private def getFromDatabase(implicit
      connection: Connection
  ): Option[QueueCall] =
    QueueCall.getByUniqueId(savedQl.callid)
  private def getFromTransferred(implicit
      repository: QueueCallRepository
  ): Option[QueueCall] =
    repository.getByTransferred(savedQl.callid)

  def tryGetQueueCall(implicit
      repository: QueueCallRepository,
      connection: Connection
  ): Flow[T, QueueCall, NotUsed] = Flow[T]
    .map { _ =>
      getFromMap
        .orElse(getFromData2)
        .orElse(getFromTransferred)
        .orElse(getFromDatabase)
    }
    .collect { case Some(queueCall) => queueCall }

  private def findLinkedIdFromData2(implicit connection: Connection) = {
    savedQl.data2 match {
      case Some(uniqueId) =>
        Cel.getLinkedIdFromUniqueId(
          uniqueId,
          "ANSWER",
          "AppQueue",
          "agentcallback"
        )
      case None => None
    }
  }
  def createQueueCall: QueueCall = QueueCall(
    id = None,
    queueRef = savedQl.queueName,
    startTime = savedQl.time,
    endTime = None,
    termination = None,
    answerTime = None,
    ringDuration = None,
    agentId = None,
    uniqueId = savedQl.callid,
    deleted = false,
    transferredCalls = Map.empty
  )
  def updateEndTime(queueCall: QueueCall): QueueCall =
    queueCall.copy(endTime = Some(savedQl.time))

}
