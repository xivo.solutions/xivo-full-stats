package xivo.fullstats.streams.queueCall.queuelog

import java.sql.Connection

import akka.NotUsed
import akka.stream.scaladsl.Flow
import org.joda.time.Seconds
import xivo.fullstats.agent.AgentPositionProxy
import xivo.fullstats.model.{AgentFeatures, ConnectQl, QueueCall}
import xivo.fullstats.streams.queueCall.{QueueCallFlow, QueueCallRepository}

class Connect(implicit repository: QueueCallRepository, connection: Connection) extends QueueCallFlow[ConnectQl] {

  val flow: Flow[ConnectQl, QueueCall, NotUsed] = Flow[ConnectQl]
    .map(keepQl)
    .map(log)
    .via(tryGetQueueCall)
    .map(resetEndTime)
    .map(updateAnswerTime)
    .map(updateAgentId)
    .map(updateRingDuration)
    .map(updateTerminationType)
    .map(finishAndSave)

  private def updateAnswerTime(queueCall: QueueCall): QueueCall = queueCall.copy(answerTime = Some(savedQl.time))

  private def updateAgentId(queueCall: QueueCall): QueueCall = {
    def extractAgentNumberFromQueueLogAgent(agent: String) = {
      if (agent.startsWith("Agent/")) {
        agent.split("/").lastOption
      }
      else {
        None
      }
    }

    extractAgentNumberFromQueueLogAgent(savedQl.agent) match {
      case Some(number) =>
        val agentId = AgentFeatures.getAgentByNumber(number).map(_.id.toLong)
        queueCall.copy(agentId = agentId)
      case None => queueCall
    }
  }

  private def updateRingDuration(queueCall: QueueCall): QueueCall = {
    queueCall.answerTime match {
      case Some(answer) => queueCall.copy(ringDuration = Some(Seconds.secondsBetween(queueCall.startTime, answer).getSeconds))
      case None => queueCall
    }
  }

  private def resetEndTime(queueCall: QueueCall): QueueCall = {
    queueCall.copy(endTime = None)
  }
}
