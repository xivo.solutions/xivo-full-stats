package xivo.fullstats.streams.queueCall.queuelog

import java.sql.Connection

import akka.NotUsed
import akka.stream.scaladsl.Flow
import xivo.fullstats.model.{CompleteQl, QueueCall, QueueTransferQl}
import xivo.fullstats.streams.queueCall.{QueueCallFlow, QueueCallRepository}

class QueueTransfer(implicit repository: QueueCallRepository, connection: Connection) extends QueueCallFlow[QueueTransferQl] {

  val flow: Flow[QueueTransferQl, QueueCall, NotUsed] = Flow[QueueTransferQl]
    .map(keepQl)
    .map(log)
    .via(tryGetQueueCall)
    .map(updateTransferredCall)
    .map(finishAndSave)

  private def updateTransferredCall(queueCall: QueueCall) = {
    savedQl.data1.map { linkedId =>
      val transferredCalls = queueCall.transferredCalls.updated(linkedId, savedQl.callid)
      queueCall.copy(transferredCalls = transferredCalls)
    }.getOrElse(queueCall)
  }
}
