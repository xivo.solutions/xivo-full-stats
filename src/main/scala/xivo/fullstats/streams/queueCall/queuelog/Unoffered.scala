package xivo.fullstats.streams.queueCall.queuelog

import akka.NotUsed
import akka.stream.scaladsl.Flow
import xivo.fullstats.model.{QueueCall, UnofferedQl}
import xivo.fullstats.streams.queueCall.{QueueCallFlow, QueueCallRepository}

import java.sql.Connection

class Unoffered(implicit
    repository: QueueCallRepository,
    connection: Connection
) extends QueueCallFlow[UnofferedQl] {

  val flow: Flow[UnofferedQl, QueueCall, NotUsed] = Flow[UnofferedQl]
    .map(keepQl)
    .map(log)
    .map(_ => createQueueCall)
    .map(updateEndTime)
    .map(updateTerminationType)
    .map(updateRingDuration)
    .map(markAsDeleted)

  private def updateRingDuration(queueCall: QueueCall): QueueCall = {
    queueCall.copy(ringDuration = Some(0))
  }

  private def markAsDeleted(queueCall: QueueCall): QueueCall = {
    queueCall.copy(deleted = true)
  }
}
