package xivo.fullstats.streams.queueCall.queuelog

import akka.NotUsed
import akka.stream.scaladsl.Flow
import xivo.fullstats.model.{EnterQueueQl, QueueCall}
import xivo.fullstats.streams.queueCall.{QueueCallFlow, QueueCallRepository}

class EnterQueue(implicit repository: QueueCallRepository)
    extends QueueCallFlow[EnterQueueQl] {

  val flow: Flow[EnterQueueQl, QueueCall, NotUsed] = Flow[EnterQueueQl]
    .map(keepQl)
    .map(log)
    .map(_ => createQueueCall)
    .map(finishAndSave)
}
