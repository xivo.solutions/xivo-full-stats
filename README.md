# XiVO stats

Statistics engine for XiVO xC environment

## Documentation

    https://documentation.xivo.solutions/en/latest/contact_center/pack_reporting/index.html

### New model 

A new statistic model is available with table prefixed `xc_`, this work is still in progress and is used for some specific reports. Relative documentation is located in **doc** folder : 
- [Initial document presenting the method and representation](doc/ACD%20-%20Reporting.odt).
- [Short presentation of the model](doc/XiVO%20CC%20new%20stats%20engine.odp)

## Development

### Run locally

Basically you just need to stop you XiVO CC full stats on your lab and launch on your host 

```
sbt run -Dreporting.address=xivocc:5443
```
where `xivocc` is your xivo cc IP.

### Run Test

Create database :

On local db or in a dockerized db

```SQL
 CREATE USER stats WITH PASSWORD 'stats';
 CREATE USER asterisk WITH PASSWORD 'asterisk';
 CREATE DATABASE asterisktest WITH OWNER asterisk;
```

```
sbt run test
```

### Script to export customer data to be able to replay full stat on it

A script [export-stats.sh](utils/export-stats.sh) is available in the utils dir.
It can be used to export a xivo_stats database to then try to re-import it on a dev environmenet to run the full stat against it.

You run it with:
```bash
./export-stats.sh path_to_backup_file "date from" "date to"
```

:warning: Take care that you need enough disk space depending on how much data you want to export.


It can be run on the reporting host at customer like this:
```bash
./export-stats.sh /var/tmp/stat-export "2020-11-01 00:00:00" "2020-11-10 00:00:00"
```
It will create:
- a file `/var/tmp/stat-export.sql` containing the *static* data (agentfeatures, ...)
- a file `/var/tmp/stat-export_cel_extract.sql` containing the `CEL` between "2020-11-01 00:00:00" and "2020-11-10 00:00:00"
- a file `/var/tmp/stat-export_queue_log_extract.sql` containing the `queue_log` between "2020-11-01 00:00:00" and "2020-11-10 00:00:00"



### Checking for invalid stats

```sql
    select days::date, t1.total_calls 
        from generate_series('2016-01-01', '2016-12-31', '1 day'::interval) as days
            left join (
                select time::date as periodic_day, sum(total) as total_calls 
                    from stat_queue_periodic 
                    where time >= '2016-01-01' and time < '2017-01-01' group by 1) as t1 
            on days = periodic_day;

    select days::date, t1.total_calls 
        from generate_series('2016-01-01', '2016-12-31', '1 day'::interval) as days
            left join (
                select queue_time::date as periodic_day, count(id) as total_calls 
                    from call_on_queue 
                        where queue_time >= '2016-01-01' and queue_time < '2017-01-01' group by 1) as t1
            on days = periodic_day;

    select days::date, t1.total_calls 
        from generate_series('2016-01-01', '2016-12-31', '1 day'::interval) as days
            left join (
                select start_time::date as periodic_day, count(id) as total_calls 
                    from call_data 
                    where start_time >= '2016-01-01' and start_time < '2017-01-01' group by 1) as t1 
            on days = periodic_day;

    select days::date, t1.total_calls 
        from generate_series('2016-01-01', '2016-12-31', '1 day'::interval) as days 
            left join (
                select start_time::date as periodic_day, count(agent_num) as total_calls 
                    from agent_position 
                    where start_time >= '2016-01-01' and start_time < '2017-01-01' group by 1) as t1 
            on days = periodic_day;
```

### Recreating stat_queue_periodic 

```sql
    insert into stat_queue_periodic(time,queue,answered,abandoned,total,"full",closed,joinempty,leaveempty,divert_ca_ratio,divert_waittime,timeout)
    select summary.time as time, summary.queue as queue, 
        sum(summary.answered) as answered, sum(summary.abandoned) as abandoned, sum(summary.total) as total, sum(summary.full) as full, 
        sum(summary.closed) as closed, sum(summary.joinempty) as joinempty, sum(summary.leaveempty) as leaveempty, 
        sum(summary.divert_ca_ratio) as divert_ca_ratio, sum(summary.divert_waitTime) as divert_waittime, sum(summary.timeout) as timeout
    from (
    select timestamp without time zone 'epoch' + (900*(extract(epoch FROM time::timestamp)::bigint /900))* interval '1 second' as time, queuename as queue, 
        sum(CASE WHEN event = 'CONNECT' THEN 1 ELSE 0 END) AS answered,
        sum(CASE WHEN event = 'ABANDON' THEN 1 ELSE 0 END) AS abandoned,
        sum(CASE WHEN event = 'ENTERQUEUE' THEN 1 ELSE 0 END) AS total,
        sum(CASE WHEN event = 'FULL' THEN 1 ELSE 0 END) AS full,
        sum(CASE WHEN event = 'CLOSED' THEN 1 ELSE 0 END) AS closed,
        sum(CASE WHEN event = 'JOINEMPTY' THEN 1 ELSE 0 END) AS joinempty,
        sum(CASE WHEN event = 'LEAVEEMPTY' THEN 1 ELSE 0 END) AS leaveempty,
        sum(CASE WHEN event = 'DIVERT_CA_RATIO' THEN 1 ELSE 0 END) AS divert_ca_ratio,
        sum(CASE WHEN event = 'DIVERT_HOLDTIME' THEN 1 ELSE 0 END) AS divert_waitTime,
        sum(CASE WHEN event = 'EXITWITHTIMEOUT' THEN 1 ELSE 0 END) AS timeout,
        sum(CASE WHEN event = 'EXITWITHKEY' THEN 1 ELSE 0 END) AS exit_with_key
        from queue_log ql where ql.time > '2016-01-01' and ql.time < '2016-01-31' and ql.queuename <> 'NONE' group by time, queue) as summary 
    group by time, queue order by time,queue;
```

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

See the [COPYING](COPYING) and [COPYING.LESSER](COPYING.LESSER) files for details.
